require 'csv'

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


User.create!(name: 'Klaus', profile: 0, email: 'klausf@gmail.com', password_digest: '123456')
User.create!(name: 'Vendedor', profile: 1, email: 'vendedor@buyimoveis.com.br', password_digest: '123456')

country = Country.create(name:'Brasil', acronym: 'BR')

State.create(country: country, acronym: 'AC', ibge_code: 12, name: 'Acre')
State.create(country: country, acronym: 'AL', ibge_code: 27, name: 'Alagoas')
State.create(country: country, acronym: 'AM', ibge_code: 13, name: 'Amazonas')
State.create(country: country, acronym: 'AP', ibge_code: 16, name: 'Amapá')
State.create(country: country, acronym: 'BA', ibge_code: 29, name: 'Bahia')
State.create(country: country, acronym: 'CE', ibge_code: 23, name: 'Ceará')
State.create(country: country, acronym: 'DF', ibge_code: 53, name: 'Distrito Federal')
State.create(country: country, acronym: 'ES', ibge_code: 32, name: 'Espírito Santo')
State.create(country: country, acronym: 'GO', ibge_code: 52, name: 'Goiás')
State.create(country: country, acronym: 'MA', ibge_code: 21, name: 'Maranhão')
State.create(country: country, acronym: 'MG', ibge_code: 31, name: 'Minas Gerais')
State.create(country: country, acronym: 'MS', ibge_code: 50, name: 'Mato Grosso do Sul')
State.create(country: country, acronym: 'MT', ibge_code: 51, name: 'Mato Grosso')
State.create(country: country, acronym: 'PA', ibge_code: 15, name: 'Pará')
State.create(country: country, acronym: 'PB', ibge_code: 25, name: 'Paraíba')
State.create(country: country, acronym: 'PE', ibge_code: 26, name: 'Pernambuco')
State.create(country: country, acronym: 'PI', ibge_code: 22, name: 'Piauí')
State.create(country: country, acronym: 'PR', ibge_code: 41, name: 'Paraná')
State.create(country: country, acronym: 'RJ', ibge_code: 33, name: 'Rio de Janeiro')
State.create(country: country, acronym: 'RN', ibge_code: 24, name: 'Rio Grande do Norte')
State.create(country: country, acronym: 'RO', ibge_code: 11, name: 'Rondônia')
State.create(country: country, acronym: 'RR', ibge_code: 14, name: 'Roraima')
State.create(country: country, acronym: 'RS', ibge_code: 43, name: 'Rio Grande do Sul')
State.create(country: country, acronym: 'SC', ibge_code: 42, name: 'Santa Catarina')
State.create(country: country, acronym: 'SE', ibge_code: 28, name: 'Sergipe')
State.create(country: country, acronym: 'SP', ibge_code: 35, name: 'São Paulo')
State.create(country: country, acronym: 'TO', ibge_code: 17, name: 'Tocantins')

CSV.foreach('./db/cities.csv') {|row|
  City.create(name: row[2], state_id: row[3].to_i)
}

CSV.foreach('./db/neighborhoods.csv') {|row|
  Neighborhood.create(name: "#{row[1]}", city_id: row[2].to_i)
}
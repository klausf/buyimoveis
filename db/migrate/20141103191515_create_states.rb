class CreateStates < ActiveRecord::Migration
  def change
    create_table :states do |t|
      t.string :acronym, :limit => 2
      t.integer :ibge_code
      t.string :name, :limit => 100, :null => false
      t.belongs_to :country, index: true

      t.timestamps
    end
  end
end

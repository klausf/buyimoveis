class CreateNewsLetters < ActiveRecord::Migration
  def change
    create_table :news_letters do |t|
      t.string :email, :limit => 200, :null => false

      t.timestamps
    end
  end
end

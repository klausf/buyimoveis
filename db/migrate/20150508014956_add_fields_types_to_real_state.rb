class AddFieldsTypesToRealState < ActiveRecord::Migration
  def change
    add_column :real_estates, :planned_neighborhood, :boolean, :default => false
    add_column :real_estates, :suite, :boolean, :default => false
    add_column :real_estates, :lavatory, :boolean, :default => false
    add_column :real_estates, :vacancy, :boolean, :default => false
    add_column :real_estates, :hot_water, :boolean, :default => false
    add_column :real_estates, :central_gas, :boolean, :default => false
    add_column :real_estates, :barbecue_condo, :boolean, :default => false
    add_column :real_estates, :anchor_to_split, :boolean, :default => false
    add_column :real_estates, :anchor_to_hood, :boolean, :default => false
    add_column :real_estates, :forest, :boolean, :default => false
    add_column :real_estates, :intercom, :boolean, :default => false
    add_column :real_estates, :individual_measurement_gas, :boolean, :default => false
    add_column :real_estates, :individual_measurement_water, :boolean, :default => false
    add_column :real_estates, :vacancy_guest, :boolean, :default => false
    add_column :real_estates, :port_cochare, :boolean, :default => false
    add_column :real_estates, :party_room, :boolean, :default => false
    add_column :real_estates, :terrace, :boolean, :default => false
    add_column :real_estates, :room_convention, :boolean, :default => false
    add_column :real_estates, :gourmet_space, :boolean, :default => false
    add_column :real_estates, :adult_pool, :boolean, :default => false
    add_column :real_estates, :children_pool, :boolean, :default => false
    add_column :real_estates, :gym, :boolean, :default => false
    add_column :real_estates, :pantry, :boolean, :default => false
    add_column :real_estates, :dressing, :boolean, :default => false
    add_column :real_estates, :dependence_maid, :boolean, :default => false
    add_column :real_estates, :tv_circuit, :boolean, :default => false
    add_column :real_estates, :automatization, :boolean, :default => false
    add_column :real_estates, :generator, :boolean, :default => false
    add_column :real_estates, :garth, :boolean, :default => false
    add_column :real_estates, :balcony, :boolean, :default => false
    add_column :real_estates, :service_area, :boolean, :default => false
    add_column :real_estates, :caretaker, :boolean, :default => false
    add_column :real_estates, :whirlpool, :boolean, :default => false
    add_column :real_estates, :lobby, :boolean, :default => false
  end

end

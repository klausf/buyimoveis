class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name, :limit => 100, :null => false
      t.string :email, :limit => 200, :null => false
      t.string :password, :limit => 100, :null => false
      t.string :password_digest, :limit => 100, :null => false
      t.boolean :removed, :null => false, :default => false
      t.integer :profile, :null => false

      t.timestamps
    end
  end
end

class AddFieldsToRealState < ActiveRecord::Migration
  def change
    add_column :real_estates, :solar_north, :boolean, :default => false
    add_column :real_estates, :solar_south, :boolean, :default => false
    add_column :real_estates, :solar_east, :boolean, :default => false
    add_column :real_estates, :solar_west, :boolean, :default => false
    add_column :real_estates, :position_back, :boolean, :default => false
    add_column :real_estates, :position_front, :boolean, :default => false
    add_column :real_estates, :negotiable, :boolean, :default => false
    add_column :real_estates, :busy, :boolean, :default => false
    add_column :real_estates, :exclusivity, :boolean, :default => false

    add_column :real_estates, :vacancy_covered, :boolean, :default => false
    add_column :real_estates, :vacancy_uncovered, :boolean, :default => false
    add_column :real_estates, :vacancy_double, :boolean, :default => false
    add_column :real_estates, :vacancy_simple, :boolean, :default => false

    add_column :real_estates, :street_notes, :string, :limit => 30

    add_column :real_estates, :construction_company, :string, :limit => 50
    add_column :real_estates, :ground_type, :string, :limit => 50
    add_column :real_estates, :service, :string, :limit => 50
    add_column :real_estates, :inside_description, :text

    add_column :real_estates, :owner_name, :string, :limit => 50
    add_column :real_estates, :owner_rg, :string, :limit => 10
    add_column :real_estates, :owner_cpf, :string, :limit => 11
    add_column :real_estates, :owner_phone, :string, :limit => 15
    add_column :real_estates, :owner_email, :string, :limit => 100

    add_column :real_estates, :bookie_name, :string, :limit => 50
    add_column :real_estates, :bookie_email, :string, :limit => 100
    add_column :real_estates, :bookie_phone, :string, :limit => 15
    add_column :real_estates, :bookie_where_keys, :string, :limit => 50

    add_column :real_estates, :square_meter_value, :decimal, :precision => 9, :scale => 2, :default => 0
    add_column :real_estates, :brokerage_value, :decimal, :precision => 9, :scale => 2, :default => 0
    add_column :real_estates, :iptu_value, :decimal, :precision => 9, :scale => 2, :default => 0

    add_column :real_estates, :front_meters, :integer, :default => 0
    add_column :real_estates, :back_meters, :integer, :default => 0

    add_column :real_estates, :construtive_index, :integer, :default => 0
    add_column :real_estates, :enrollment_number, :integer, :default => 0
    add_column :real_estates, :enrollment_vacation_number, :integer, :default => 0
    add_column :real_estates, :registration_zone, :integer, :default => 0

    add_column :real_estates, :elevators, :integer, :limit => 1, :default => 0
    add_column :real_estates, :rooms, :integer, :limit => 1, :default => 0
    add_column :real_estates, :balconies, :integer, :limit => 1, :default => 0
    add_column :real_estates, :kitchens, :integer, :limit => 1, :default => 0
    add_column :real_estates, :year_construction, :integer, :limit => 2, :default => 0

    add_column :real_estates, :conservation, :integer, :limit => 1, :default => 0
    add_column :real_estates, :furnished, :integer, :limit => 1, :default => 0
    add_column :real_estates, :towers_count, :integer, :limit => 1, :default => 0
    add_column :real_estates, :apartment_floor_count, :integer, :limit => 1, :default => 0
    add_column :real_estates, :floor_count, :integer, :limit => 2, :default => 0
    add_column :real_estates, :apartment_count, :integer, :limit => 2, :default => 0

  end
end

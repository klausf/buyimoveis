class CreateCities < ActiveRecord::Migration
  def change
    create_table :cities do |t|
      t.string :name, :limit => 100, :null => false
      t.belongs_to :state, index: true

      t.timestamps
    end
  end
end
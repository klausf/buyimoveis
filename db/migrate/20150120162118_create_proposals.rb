class CreateProposals < ActiveRecord::Migration
  def change
    create_table :proposals do |t|
      t.belongs_to :user_created_by, :class_name => 'User', :foreign_key => 'user_created_by_id', index:true
      t.belongs_to :user_changed_by, :class_name => 'User', :foreign_key => 'user_changed_by_id', index:true

      t.integer :code, :limit => 5, :null => false
      t.text :notes, :null => false
      t.string :key, :limit => 32, :null => false
      t.string :email, :limit => 200, :null => false
      t.belongs_to :real_estate, index: true, :null => false
      t.boolean :removed, :null => false, :default => false

      t.timestamps
    end
  end
end

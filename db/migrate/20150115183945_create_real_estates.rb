class CreateRealEstates < ActiveRecord::Migration
  def change
    create_table :real_estates do |t|
      t.belongs_to :user_created_by, :class_name => 'User', :foreign_key => 'user_created_by_id', index: true
      t.belongs_to :user_changed_by, :class_name => 'User', :foreign_key => 'user_changed_by_id', index: true

      t.belongs_to :neighborhood, :null => false, index: true

      t.boolean :apartment_complex, :null => false, :default => false
      t.boolean :barbecue_grill, :null => false, :default => false
      t.boolean :concierge, :null => false, :default => false
      t.boolean :elevator, :null => false, :default => false
      t.boolean :fireplace, :null => false, :default => false
      t.boolean :playground, :null => false, :default => false
      t.boolean :removed, :null => false, :default => false

      t.integer :rent_type, :limit => 1, :null => false
      t.integer :area_usable, :null => false, :default => 0
      t.integer :bathrooms, :limit => 1, :null => false, :default => 0
      t.integer :bedrooms, :limit => 1, :null => false, :default => 0
      t.integer :parking_spaces, :limit => 1, :null => false, :default => 0
      t.integer :street_number, :null => false, :default => 0
      t.integer :total_area, :null => false, :default => 0

      t.decimal :apartment_complex_price, :precision => 9, :scale => 2, :default => 0
      t.decimal :price, :precision => 9, :scale => 2, :default => 0, :null => false
      t.string :latitude, :limit => 20, :null => false
      t.string :longitude, :limit => 20, :null => false

      t.string :title, :limit => 100, :null => false
      t.string :postal_code, :limit => 8
      t.string :street, :limit => 100

      t.text :notes

      t.timestamps
    end
  end
end

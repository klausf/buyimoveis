class CreateCountries < ActiveRecord::Migration
  def change
    create_table :countries do |t|
      t.string :name, :limit => 40, :null => false
      t.string :acronym, :limit => 2, :null => false

      t.timestamps
    end
  end
end

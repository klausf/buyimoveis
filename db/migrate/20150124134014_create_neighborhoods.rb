class CreateNeighborhoods < ActiveRecord::Migration
  def change
    create_table :neighborhoods do |t|
      t.string :name, :length => 100, :null => false
      t.belongs_to :city, :null => false, index: true

      t.timestamps
    end
  end
end

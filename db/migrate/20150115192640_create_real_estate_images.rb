class CreateRealEstateImages < ActiveRecord::Migration
  def change
    create_table :real_estate_images do |t|
      t.belongs_to :user_created_by, :class_name => 'User', :foreign_key => 'user_created_by_id', index: true
      t.belongs_to :user_changed_by, :class_name => 'User', :foreign_key => 'user_changed_by_id', index: true
      t.belongs_to :real_estate, :null => false, index: true

      t.string :label, :limit => 200
      t.string :url, :limit => 200

      t.timestamps
    end
  end
end

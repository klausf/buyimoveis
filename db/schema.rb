# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150508141654) do

  create_table "cities", force: true do |t|
    t.string   "name",       limit: 100, null: false
    t.integer  "state_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "cities", ["state_id"], name: "index_cities_on_state_id", using: :btree

  create_table "countries", force: true do |t|
    t.string   "name",       limit: 40, null: false
    t.string   "acronym",    limit: 2,  null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "neighborhoods", force: true do |t|
    t.string   "name",       null: false
    t.integer  "city_id",    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "neighborhoods", ["city_id"], name: "index_neighborhoods_on_city_id", using: :btree

  create_table "news_letters", force: true do |t|
    t.string   "email",      limit: 200, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "proposals", force: true do |t|
    t.integer  "user_created_by_id"
    t.integer  "user_changed_by_id"
    t.integer  "code",               limit: 8,                   null: false
    t.text     "notes",                                          null: false
    t.string   "key",                limit: 32,                  null: false
    t.string   "email",              limit: 200,                 null: false
    t.integer  "real_estate_id",                                 null: false
    t.boolean  "removed",                        default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "proposals", ["real_estate_id"], name: "index_proposals_on_real_estate_id", using: :btree
  add_index "proposals", ["user_changed_by_id"], name: "index_proposals_on_user_changed_by_id", using: :btree
  add_index "proposals", ["user_created_by_id"], name: "index_proposals_on_user_created_by_id", using: :btree

  create_table "real_estate_images", force: true do |t|
    t.integer  "user_created_by_id"
    t.integer  "user_changed_by_id"
    t.integer  "real_estate_id",                 null: false
    t.string   "label",              limit: 200
    t.string   "url",                limit: 200
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "real_estate_images", ["real_estate_id"], name: "index_real_estate_images_on_real_estate_id", using: :btree
  add_index "real_estate_images", ["user_changed_by_id"], name: "index_real_estate_images_on_user_changed_by_id", using: :btree
  add_index "real_estate_images", ["user_created_by_id"], name: "index_real_estate_images_on_user_created_by_id", using: :btree

  create_table "real_estates", force: true do |t|
    t.integer  "user_created_by_id"
    t.integer  "user_changed_by_id"
    t.integer  "neighborhood_id",                                                                  null: false
    t.boolean  "apartment_complex",                                                default: false, null: false
    t.boolean  "barbecue_grill",                                                   default: false, null: false
    t.boolean  "concierge",                                                        default: false, null: false
    t.boolean  "elevator",                                                         default: false, null: false
    t.boolean  "fireplace",                                                        default: false, null: false
    t.boolean  "playground",                                                       default: false, null: false
    t.boolean  "removed",                                                          default: false, null: false
    t.integer  "rent_type",                    limit: 1,                                           null: false
    t.integer  "area_usable",                                                      default: 0,     null: false
    t.integer  "bathrooms",                    limit: 1,                           default: 0,     null: false
    t.integer  "bedrooms",                     limit: 1,                           default: 0,     null: false
    t.integer  "parking_spaces",               limit: 1,                           default: 0,     null: false
    t.integer  "street_number",                                                    default: 0,     null: false
    t.integer  "total_area",                                                       default: 0,     null: false
    t.decimal  "apartment_complex_price",                  precision: 9, scale: 2, default: 0.0
    t.decimal  "price",                                    precision: 9, scale: 2, default: 0.0,   null: false
    t.string   "latitude",                     limit: 20,                                          null: false
    t.string   "longitude",                    limit: 20,                                          null: false
    t.string   "title",                        limit: 100,                                         null: false
    t.string   "postal_code",                  limit: 8
    t.string   "street",                       limit: 100
    t.text     "notes"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "planned_neighborhood",                                             default: false
    t.boolean  "suite",                                                            default: false
    t.boolean  "lavatory",                                                         default: false
    t.boolean  "vacancy",                                                          default: false
    t.boolean  "hot_water",                                                        default: false
    t.boolean  "central_gas",                                                      default: false
    t.boolean  "barbecue_condo",                                                   default: false
    t.boolean  "anchor_to_split",                                                  default: false
    t.boolean  "anchor_to_hood",                                                   default: false
    t.boolean  "forest",                                                           default: false
    t.boolean  "intercom",                                                         default: false
    t.boolean  "individual_measurement_gas",                                       default: false
    t.boolean  "individual_measurement_water",                                     default: false
    t.boolean  "vacancy_guest",                                                    default: false
    t.boolean  "port_cochare",                                                     default: false
    t.boolean  "party_room",                                                       default: false
    t.boolean  "terrace",                                                          default: false
    t.boolean  "room_convention",                                                  default: false
    t.boolean  "gourmet_space",                                                    default: false
    t.boolean  "adult_pool",                                                       default: false
    t.boolean  "children_pool",                                                    default: false
    t.boolean  "gym",                                                              default: false
    t.boolean  "pantry",                                                           default: false
    t.boolean  "dressing",                                                         default: false
    t.boolean  "dependence_maid",                                                  default: false
    t.boolean  "tv_circuit",                                                       default: false
    t.boolean  "automatization",                                                   default: false
    t.boolean  "generator",                                                        default: false
    t.boolean  "garth",                                                            default: false
    t.boolean  "balcony",                                                          default: false
    t.boolean  "service_area",                                                     default: false
    t.boolean  "caretaker",                                                        default: false
    t.boolean  "whirlpool",                                                        default: false
    t.boolean  "lobby",                                                            default: false
    t.boolean  "solar_north",                                                      default: false
    t.boolean  "solar_south",                                                      default: false
    t.boolean  "solar_east",                                                       default: false
    t.boolean  "solar_west",                                                       default: false
    t.boolean  "position_back",                                                    default: false
    t.boolean  "position_front",                                                   default: false
    t.boolean  "negotiable",                                                       default: false
    t.boolean  "busy",                                                             default: false
    t.boolean  "exclusivity",                                                      default: false
    t.boolean  "vacancy_covered",                                                  default: false
    t.boolean  "vacancy_uncovered",                                                default: false
    t.boolean  "vacancy_double",                                                   default: false
    t.boolean  "vacancy_simple",                                                   default: false
    t.string   "construction_company",         limit: 50
    t.string   "ground_type",                  limit: 50
    t.string   "service",                      limit: 50
    t.text     "inside_description"
    t.string   "owner_name",                   limit: 50
    t.string   "owner_rg",                     limit: 10
    t.string   "owner_cpf",                    limit: 11
    t.string   "owner_phone",                  limit: 15
    t.string   "owner_email",                  limit: 100
    t.string   "bookie_name",                  limit: 50
    t.string   "bookie_email",                 limit: 100
    t.string   "bookie_phone",                 limit: 15
    t.string   "bookie_where_keys",            limit: 50
    t.decimal  "square_meter_value",                       precision: 9, scale: 2, default: 0.0
    t.decimal  "brokerage_value",                          precision: 9, scale: 2, default: 0.0
    t.decimal  "iptu_value",                               precision: 9, scale: 2, default: 0.0
    t.integer  "front_meters",                                                     default: 0
    t.integer  "back_meters",                                                      default: 0
    t.integer  "construtive_index",                                                default: 0
    t.integer  "enrollment_number",                                                default: 0
    t.integer  "enrollment_vacation_number",                                       default: 0
    t.integer  "registration_zone",                                                default: 0
    t.integer  "elevators",                    limit: 1,                           default: 0
    t.integer  "rooms",                        limit: 1,                           default: 0
    t.integer  "balconies",                    limit: 1,                           default: 0
    t.integer  "kitchens",                     limit: 1,                           default: 0
    t.integer  "year_construction",            limit: 2,                           default: 0
    t.integer  "conservation",                 limit: 1,                           default: 0
    t.integer  "furnished",                    limit: 1,                           default: 0
    t.integer  "towers_count",                 limit: 1,                           default: 0
    t.integer  "apartment_floor_count",        limit: 1,                           default: 0
    t.integer  "floor_count",                  limit: 2,                           default: 0
    t.integer  "apartment_count",              limit: 2,                           default: 0
  end

  add_index "real_estates", ["neighborhood_id"], name: "index_real_estates_on_neighborhood_id", using: :btree
  add_index "real_estates", ["user_changed_by_id"], name: "index_real_estates_on_user_changed_by_id", using: :btree
  add_index "real_estates", ["user_created_by_id"], name: "index_real_estates_on_user_created_by_id", using: :btree

  create_table "states", force: true do |t|
    t.string   "acronym",    limit: 2
    t.integer  "ibge_code"
    t.string   "name",       limit: 100, null: false
    t.integer  "country_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "states", ["country_id"], name: "index_states_on_country_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "name",            limit: 100,                 null: false
    t.string   "email",           limit: 200,                 null: false
    t.string   "password",        limit: 100,                 null: false
    t.string   "password_digest", limit: 100,                 null: false
    t.boolean  "removed",                     default: false, null: false
    t.integer  "profile",                                     null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end

<% if namespaced? -%>
require_dependency "<%= namespaced_file_path %>/application_controller"

<% end -%>
<% module_namespacing do -%>
class <%= controller_class_name %>Controller < ApplicationController
  before_action :set_<%= singular_table_name %>, only: [:edit, :update, :destroy]

  # GET <%= route_url %>
  def index
    respond_to do |f|
      f.html { render :layout => params[:l].nil? }
      f.json {

        @<%= plural_table_name %> = <%= class_name %>.paginate(:page => params[:page], :per_page => 15).order('id asc')

        render :json => {
            :current_page => @<%= plural_table_name %>.current_page,
            :per_page => @<%= plural_table_name %>.per_page,
            :total_entries => @<%= plural_table_name %>.total_entries,
            :entries => @<%= plural_table_name %>
        }
      }
    end

  end


  # GET <%= route_url %>/1
  def show
  end

  # GET <%= route_url %>/new
  def new
    @<%= singular_table_name %> = <%= orm_class.build(class_name) %>
    render :layout => params[:l].nil?
  end

  # GET <%= route_url %>/1/edit
  def edit
    respond_to do |f|
      f.html { render :layout => params[:l].nil? }
      f.json {
        set_<%= singular_table_name %>
        render :json => @<%= singular_table_name %>
      }
    end
  end

  # POST <%= route_url %>
  def create
    @<%= singular_table_name %> = <%= orm_class.build(class_name, "#{singular_table_name}_params") %>

    respond_to do |f|
      if @<%= singular_table_name %>.save
        f.html { redirect_to @<%= singular_table_name %>, notice: '<%= class_name %> criado(a) com sucesso.' }
        f.json { render :show, status: :created, location: @<%= singular_table_name %> }
      else
        f.html { render :new }
        f.json { render json: @<%= singular_table_name %>.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT <%= route_url %>/1
  def update
    respond_to do |f|
      if @<%= orm_instance.update("#{singular_table_name}_params") %>
        f.html { redirect_to @<%= singular_table_name %>, notice: '<%= class_name %> alterado(a) com sucesso.' }
        f.json { render :show, status: :ok, location: @<%= singular_table_name %> }
      else
        f.html { render :edit }
        f.json { render json: @<%= singular_table_name %>.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE <%= route_url %>/1
  def destroy
    @<%= orm_instance.destroy %>
    respond_to do |f|
      f.html { redirect_to <%= index_helper %>_url, notice: <%= "'#{class_name} removido(a) com sucesso.'" %> }
      f.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_<%= singular_table_name %>
      @<%= singular_table_name %> = <%= orm_class.find(class_name, "params[:id]") %>
    end

    # Only allow a trusted parameter "white list" through.
    def <%= "#{singular_table_name}_params" %>
      <%- if attributes_names.empty? -%>
      params[<%= ":#{singular_table_name}" %>]
      <%- else -%>
      params.require(<%= ":#{singular_table_name}" %>).permit(<%= attributes_names.map { |name| ":#{name}" }.join(', ') %>)
      <%- end -%>
    end
end
<% end -%>

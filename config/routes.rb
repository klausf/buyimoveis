Rails.application.routes.draw do

  get 'sitemap.:format' => 'home#sitemap'

  get 'sobre' => 'about_us#index', :as => 'about_us'
  get 'contato' => 'contact#index', :as => 'contact'
  post 'contato' => 'contact#contact_us', :as => 'contact_us'

  get 'imovel/:id/:estate/:city/:title' => 'look_real_estate#index', :as => 'look_real_estate_index'
  get 'imovel/codigo/:code' => 'look_real_estate#by_code', :as => 'look_real_estate_index_code'
  get 'imovel/proposta/:key' => 'look_real_estate#proposal', :as => 'look_real_estate_proposal'
  post 'imovel/solicitar-proposta' => 'look_real_estate#call_proposal', :as => 'look_real_estate_call_proposal'

  get 'procurar' => 'search#index', :as => 'search'
  get 'procurar/tipo/:name/:page' => 'search#type', :as => 'search_by_type'

  get 'locations/all-states' => 'application#load_states'
  get 'locations/search-cities/:state_id' => 'application#search_cities'
  get 'locations/search-cities-by-name/:q' => 'application#search_cities_by_name'
  get 'locations/search-neighborhoods/:q' => 'application#search_neighborhoods'
  get 'locations/search-neighborhoods-by-city/:city_id' => 'application#search_neighborhoods_by_city'
  get 'locations/search-postal-code/:postal_code' => 'application#search_postal_code'
  post 'news-letter/subscribe' => 'news_letters#subscribe', :as => 'subscribe'


  get 'manager' => 'manager_home#index'

  scope :manager do
    resources :real_estates do
      collection do
        post 'upload-pic', :to => 'real_estates#upload_pic', :as => 'upload_pic'
        delete 'delete-pic/:key', :to => 'real_estates#delete_pic', :as => 'delete_pic'
      end
    end

    resources :users do
      collection do
        get 'profile', :to => 'users#profile', :as => 'profile'
      end
    end

    resources :proposals do
      collection do
        get 'real-estate/:id', :to => 'proposals#real_estate', :as => 'real_estate'
        get 'send-proposal/:id', :to => 'proposals#send_proposal', :as => 'send_proposal'
      end
    end

    resources :cities, :neighborhoods, :news_letters

    get 'session' => 'session#index'
    post 'session/create' => 'session#create'
    get 'session/destroy' => 'session#destroy'
    post 'session/forget-password' => 'session#forget_password', :defaults => { :format => 'json' }
  end

  root 'home#index'


end

/**
 * Created by Picanha on 24/01/2015.
 */
_buy.config(function ($routeProvider) {
    $routeProvider.when('/manager/neighborhoods', {
        templateUrl: '/manager/neighborhoods.html?l=t',
        controller: 'NeighborhoodSearchCtrl'
    }).when('/manager/neighborhoods/new', {
        templateUrl: '/manager/neighborhoods/new.html?l=t',
        controller: 'NeighborhoodCtrl'
    }).when('/manager/neighborhoods/profile', {
        templateUrl: '/manager/neighborhoods/profile.html?l=t',
        controller: 'NeighborhoodCtrl'
    }).when('/manager/neighborhoods/:id/edit', {
        templateUrl: function(params){return '/manager/neighborhoods/' + params.id + '/edit.html?l=t'},
        controller: 'NeighborhoodCtrl'
    });

}).factory('Neighborhood', ['railsResourceFactory', 'railsSerializer', function (railsResourceFactory, railsSerializer) {
    return railsResourceFactory({
        url: '/manager/neighborhoods',
        name: 'neighborhood'
    });
}]).controller('NeighborhoodSearchCtrl', function($rootScope, $scope, $routeParams, Neighborhood) {
    $rootScope.titlePage = "Manager - Bairros";
    BaseCrudCtrl.call(this, $scope, $routeParams, Neighborhood);

    var initComponents = function() {
        $('[rel=panels]').panels();
    };

    initComponents();

    $scope.resetSearchForm = function() {
        $scope.searchParams = {
            keyWord : ''
        };
        angular.element('#keyWord').focus();
    };

    $scope.resetSearchForm();

    $scope.search();

}).controller('NeighborhoodCtrl', function($rootScope, $scope, $http, $routeParams, Neighborhood) {
    $rootScope.titlePage = "Manager - Gestão de Bairros";
    BaseCrudCtrl.call(this, $scope, $routeParams, Neighborhood);

    $scope.resetSaveForm = function(form) {
        if(form) form.$setPristine();
        $scope.entity = {city: {}};
        $('#name').focus();
    };

    var loadStates = function(){
        $http.get('/locations/all-states.json', { cache: true}).success(function(data, status, headers, config) {
            $scope.states = data;
        }).error(function(data, status, headers, config) {
            AppToast.showDanger(AppStorage.getUser().name, 'Aconteceu algum problema ao tentar carregar os estados. Recarregue a página clicando <a href="javascript:location.reload()">aqui</a> e se o problema persistir, contate a administração.');
        });
    };

    $scope.loadCities = function(){
        $http.get('/locations/search-cities/' + $scope.entity.stateId, { cache: true}).success(function(data, status, headers, config) {
            $scope.cities = data;
            if($scope.city != '') {
                setTimeout(function(){
                    $('#cities option:contains('+ $scope.city +'):first').prop('selected', true).trigger('change');
                    $scope.city = null;
                }, 30);
            }

        }).error(function(data, status, headers, config) {
            AppToast.showDanger(AppStorage.getUser().name, 'Aconteceu algum problema ao tentar carregar as cidades. Recarregue a página clicando <a href="javascript:location.reload()">aqui</a> e se o problema persistir, contate a administração.');
        });
    };

    var initComponents = function() {
        $('[rel=panels]').panels();
    };

    $scope.afterLoadEntity = function() {
        //console.log($scope.entity);
        $scope.entity.stateId = $scope.entity.city.state.id;
        $scope.entity.city = $scope.entity.city.name;
        $scope.loadCities();
    };

    loadStates();

    initComponents();
    $scope.loadEntity();

    if($routeParams.id === undefined) {
        $scope.resetSaveForm();
    } else {
        $('#name').focus();
    }
});
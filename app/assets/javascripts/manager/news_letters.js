_buy.config(function ($routeProvider) {
    $routeProvider.when('/manager/news_letters', {
        templateUrl: '/manager/news_letters.html?l=t',
        controller: 'NewsLetterSearchCtrl'
    }).when('/manager/news_letters/new', {
        templateUrl: '/manager/news_letters/new.html?l=t',
        controller: 'NewsLetterCtrl'
    }).when('/manager/news_letters/:id/edit', {
        templateUrl: function(params){return '/manager/news_letters/' + params.id + '/edit.html?l=t'},
        controller: 'NewsLetterCtrl'
    });

}).factory('NewsLetter', ['railsResourceFactory', 'railsSerializer', function (railsResourceFactory, railsSerializer) {
    return railsResourceFactory({
        url: '/manager/news_letters',
        name: 'newsLetter'
    });
}]).controller('NewsLetterSearchCtrl', function($rootScope, $scope, $routeParams, NewsLetter) {
    $rootScope.titlePage = "Manager - Pesquisa de Newsletter";
    BaseCrudCtrl.call(this, $scope, $routeParams, NewsLetter);

    var initComponents = function() {
        $('[rel=panels]').panels();
    };

    initComponents();

    $scope.resetSearchForm = function() {
        $scope.searchParams = {
            keyWord : ''
        };
        angular.element('#email').focus();
    };

    $scope.resetSearchForm();

    $scope.search();

}).controller('NewsLetterCtrl', function($rootScope, $scope, $http, $routeParams, NewsLetter) {
    $rootScope.titlePage = "Manager - Gestão de Newsletter";
    BaseCrudCtrl.call(this, $scope, $routeParams, NewsLetter);

    $scope.resetSaveForm = function(form) {
        if(form) form.$setPristine();
        $scope.entity = {};
        $('#name').focus();
    };

    var initComponents = function() {
        $('[rel=panels]').panels();
    };

    initComponents();

    $scope.loadEntity();

    if($routeParams.id === undefined) {
        $scope.resetSaveForm();
    } else {
        $('#email').focus();
    }
});
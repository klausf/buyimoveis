_buy.config(function ($routeProvider) {
    $routeProvider.when('/manager/proposals', {
        templateUrl: '/manager/proposals.html?l=t',
        controller: 'ProposalSearchCtrl'
    }).when('/manager/proposals/new', {
        templateUrl: '/manager/proposals/new.html?l=t',
        controller: 'ProposalCtrl'
    }).when('/manager/proposals/:id/edit', {
        templateUrl: function(params){return '/manager/proposals/' + params.id + '/edit.html?l=t'},
        controller: 'ProposalCtrl'
    });
}).factory('Proposal', ['railsResourceFactory', 'railsSerializer', function (railsResourceFactory, railsSerializer) {
    return railsResourceFactory({
        url: '/manager/proposals',
        name: 'proposal'
    });
}]).controller('ProposalSearchCtrl', function($rootScope, $scope, $routeParams, Proposal) {
    $rootScope.titlePage = "Manager - Pesquisa de Propostas";
    BaseCrudCtrl.call(this, $scope, $routeParams, Proposal);

    var initComponents = function() {
        $('[rel=panels]').panels();
    };

    initComponents();

    $scope.resetSearchForm = function() {
        $scope.searchParams = {

        };
        angular.element('#code').focus();
    };

    $scope.resetSearchForm();

    $scope.search();

}).controller('ProposalCtrl', function($rootScope, $scope, $http, $routeParams, Proposal) {
    $rootScope.titlePage = "Manager - Gestão de Propostas";
    BaseCrudCtrl.call(this, $scope, $routeParams, Proposal);

    $scope.resetSaveForm = function(form) {
        if(form) form.$setPristine();
        $scope.entity = {
            notes: 'Entrada de R$   +\n'
                    + 'X  de  R$    (parcelas)\n'
                    + 'Financiamento  de R$     '
        };
        $('#title').focus();
    };


    var initComponents = function() {
        $('[rel=panels]').panels();
    };

    $scope.send = function() {
        $http.get('/manager/proposals/send-proposal/' + $scope.entity.id).success(function(data, status, headers, config) {
            AppToast.showInfo(AppStorage.getUser().name, "Proposta enviada com sucesso!");
        }).error(function() {
            AppToast.showDanger(AppStorage.getUser().name, 'Aconteceu algum problema ao tentar enviar a proposta.');
        });
    };

    initComponents();
    $scope.loadEntity();

    if ($routeParams.by !== undefined) {
        $http.get('/manager/proposals/real-estate/' + $routeParams.by).success(function(data, status, headers, config) {
            $scope.entity.realEstate = data;
            $scope.entity.realEstateId = data.id
        }).error(function(data, status, headers, config) {
            AppToast.showDanger(AppStorage.getUser().name, 'Aconteceu algum problema ao tentar carregar o imóvel. Recarregue a página clicando <a href="javascript:location.reload()">aqui</a> e se o problema persistir, contate a administração.');
        });
    }

    if($routeParams.id === undefined) {
        $scope.resetSaveForm();
    } else {
        $('#title').focus();
    }
});
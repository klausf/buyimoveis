_buy.config(function ($routeProvider) {
    $routeProvider.when('/manager', {
        templateUrl: '/manager.html?l=t',
        controller: 'HomeCtrl'
    });
}).controller('HomeCtrl', function($rootScope, $scope, $http, $location){
    $rootScope.titlePage = "Manager - Dashboard";

    var init = function() {
        $('[rel=panels]').panels();
    };


    init();
});
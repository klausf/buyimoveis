_buy.config(function ($routeProvider) {
    $routeProvider.when('/manager/session', {
        templateUrl: '/manager/session.html?l=t',
        controller: 'LoginCtrl'
    });
}).controller('LoginCtrl', function($rootScope, $scope, $http){
    $rootScope.titlePage = "Manager - Login";

    $scope.credentials = {
        email: AppStorage.getUser().email
    };

    $scope.login = function(credentials) {
        $scope.messageHeader= 'Aguarde';
        $scope.messageProcessing= 'Efetuando login...';
        $('.actionable').addClass('actioning');
        $('.actionable-animated-fadeInLeftSmall').addClass('animated fadeInLeftSmall');
        $('.actionable-animated-fadeInRightSmall').addClass('animated fadeInRightSmall');

        setTimeout(function(){
            $http.post('/manager/session/create', credentials).success(function(data, status, headers, config) {
                $scope.messageHeader= 'Bem-vindo!';
                $scope.messageProcessing= 'Aguarde... Abrindo a administração.';

                AppStorage.saveUser(data);
                document.location.href = '/manager';
            }).error(function(data, status, headers, config) {
                $('.actionable').removeClass('actioning');
                $('.actionable-animated-fadeInLeftSmall').removeClass('animated fadeInLeftSmall');
                $('.actionable-animated-fadeInRightSmall').removeClass('animated fadeInRightSmall');

                if(status == 401) {
                    AppToast.showWarn("Ops...", 'E-mail e/ou senha inválido(s). Verifique os dados informados e tente novamente.');
                } else {
                    AppToast.showDanger("Ops...", HttpMsgs.getMsg(status));
                }
            });
        }, 2000);
    };

    $scope.setEmail = function(credential) {
        $scope.user = {
            email: credential.email
        }
    };

    $scope.forgetPassword = function(user) {
        angular.element('#forgetMail').addClass('ui-autocomplete-loading');
        $scope.isDisabled = true;

        $http.post('/manager/session/forget-password', {'user': user})
            .success(function() {
                $scope.isDisabled = false;
                angular.element('#forgetMail').removeClass('ui-autocomplete-loading');
                AppToast.showInfo("Mensagem enviada", 'Verifique sua caixa de correio para usar sua nova senha de acesso.');
                $('#myModal').modal('hide');

            }).error(function(data, status) {
                $scope.isDisabled = false;
                angular.element('#forgetMail').removeClass('ui-autocomplete-loading');
                if(status == 401) {
                    if(typeof(data.data) == 'object') {
                        AppToast.showValidMsg(data.data);
                    } else {
                        AppToast.showUnknowMsg();
                    }
                } else {
                    AppToast.showDanger("Ops...", HttpMsgs.getMsg(status));
                }
            });
    };
    angular.element('#email').focus();
});
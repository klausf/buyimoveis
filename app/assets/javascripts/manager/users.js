_buy.config(function ($routeProvider) {
    $routeProvider.when('/manager/users', {
        templateUrl: '/manager/users.html?l=t',
        controller: 'UserSearchCtrl'
    }).when('/manager/users/new', {
        templateUrl: '/manager/users/new.html?l=t',
        controller: 'UserCtrl'
    }).when('/manager/users/profile', {
        templateUrl: '/manager/users/profile.html?l=t',
        controller: 'UserProfileCtrl'
    }).when('/manager/users/:id/edit', {
        templateUrl: function(params){return '/manager/users/' + params.id + '/edit.html?l=t'},
        controller: 'UserCtrl'
    });

}).factory('User', ['railsResourceFactory', 'railsSerializer', function (railsResourceFactory, railsSerializer) {
    return railsResourceFactory({
        url: '/manager/users',
        name: 'user'
    });
}]).controller('UserSearchCtrl', function($rootScope, $scope, $routeParams, User) {
    $rootScope.titlePage = "Manager - Usuários";
    BaseCrudCtrl.call(this, $scope, $routeParams, User);

    var initComponents = function() {
        $('[rel=panels]').panels();
    };

    initComponents();

    $scope.resetSearchForm = function() {
        $scope.searchParams = {
            keyWord : ''
            ,profile : ''
        };
        angular.element('#name').focus();
    };

    $scope.resetSearchForm();

    $scope.search();

}).controller('UserCtrl', function($rootScope, $scope, $http, $routeParams, User) {
    $rootScope.titlePage = "Manager - Gestão de Usuários";
    BaseCrudCtrl.call(this, $scope, $routeParams, User);

    $scope.resetSaveForm = function(form) {
        if(form) form.$setPristine();
        $scope.entity = {
            profile : 'admin'
            ,password : ''
        };
        $('#name').focus();
    };


    var initComponents = function() {
        $('[rel=panels]').panels();
    };

    initComponents();

    $scope.loadEntity();

    if($routeParams.id === undefined) {
        $scope.resetSaveForm();
    } else {
        $('#name').focus();
    }
}).controller('UserProfileCtrl', function($rootScope, $scope, $http, $routeParams, User) {
    $rootScope.titlePage = "Manager - Meus dados";
    BaseCrudCtrl.call(this, $scope, $routeParams, User);

    $scope.resetSaveForm = function(form) {
        if(form) form.$setPristine();
        $scope.entity = {
            profile : 'admin'
            ,password : ''
            ,newPass : ''
            ,newPassConfirm : ''
        };
        $('#name').focus();
    };


    var initComponents = function() {
        $('[rel=panels]').panels();

        $http.get('/manager/users/profile.json').success(function(data, status, headers, config) {
            $scope.entity = data;
        }).error(function() {
            AppToast.showDanger(AppStorage.getUser().name, 'Aconteceu algum problema ao tentar os seus dados. Recarregue a página clicando <a href="javascript:location.reload()">aqui</a> e se o problema persistir, contate a administração.');
        });
    };

    initComponents();

});
/**
 * Created by Picanha on 24/01/2015.
 */
/**
 * Created by Picanha on 24/01/2015.
 */
_buy.config(function ($routeProvider) {
    $routeProvider.when('/manager/cities', {
        templateUrl: '/manager/cities.html?l=t',
        controller: 'CitySearchCtrl'
    }).when('/manager/cities/new', {
        templateUrl: '/manager/cities/new.html?l=t',
        controller: 'CityCtrl'
    }).when('/manager/cities/profile', {
        templateUrl: '/manager/cities/profile.html?l=t',
        controller: 'CityCtrl'
    }).when('/manager/cities/:id/edit', {
        templateUrl: function(params){return '/manager/cities/' + params.id + '/edit.html?l=t'},
        controller: 'CityCtrl'
    });

}).factory('City', ['railsResourceFactory', 'railsSerializer', function (railsResourceFactory, railsSerializer) {
    return railsResourceFactory({
        url: '/manager/cities',
        name: 'city'
    });
}]).controller('CitySearchCtrl', function($rootScope, $scope, $routeParams, City) {
    $rootScope.titlePage = "Manager - Cidades";
    BaseCrudCtrl.call(this, $scope, $routeParams, City);

    var initComponents = function() {
        $('[rel=panels]').panels();
    };

    initComponents();

    $scope.resetSearchForm = function() {
        $scope.searchParams = {
            keyWord : ''
        };
        angular.element('#keyWord').focus();
    };

    $scope.resetSearchForm();

    $scope.search();

}).controller('CityCtrl', function($rootScope, $scope, $http, $routeParams, City) {
    $rootScope.titlePage = "Manager - Gestão de Cidades";
    BaseCrudCtrl.call(this, $scope, $routeParams, City);

    $scope.resetSaveForm = function(form) {
        if(form) form.$setPristine();
        $scope.entity = {state: {}};
        $('#name').focus();
    };

    var loadStates = function(){
        $http.get('/locations/all-states.json', { cache: true}).success(function(data) {
            $scope.states = data;
        }).error(function() {
            AppToast.showDanger(AppStorage.getUser().name, 'Aconteceu algum problema ao tentar carregar os estados. Recarregue a página clicando <a href="javascript:location.reload()">aqui</a> e se o problema persistir, contate a administração.');
        });
    };



    var initComponents = function() {
        $('[rel=panels]').panels();
    };

    $scope.afterLoadEntity = function() {
        $('#states option[value="' + $scope.stateId +'"]').prop('selected', true);
    };

    initComponents();


    $scope.loadEntity();

    loadStates();

    if($routeParams.id === undefined) {
        $scope.resetSaveForm();
    } else {
        $('#name').focus();
    }
});

_buy.config(function ($routeProvider) {
    $routeProvider.when('/manager/real_estates', {
        templateUrl: '/manager/real_estates.html?l=t',
        controller: 'RealEstateSearchCtrl'
    }).when('/manager/real_estates/new', {
        templateUrl: '/manager/real_estates/new.html?l=t',
        controller: 'RealEstateCtrl'
    }).when('/manager/real_estates/:id/edit', {
        templateUrl: function (params) {
            return '/manager/real_estates/' + params.id + '/edit.html?l=t'
        },
        controller: 'RealEstateCtrl'
    });
}).factory('RealEstate', ['railsResourceFactory', 'railsSerializer', function (railsResourceFactory, railsSerializer) {
    return railsResourceFactory({
        url: '/manager/real_estates',
        name: 'realEstate'
        , serializer: railsSerializer(function () {
            this.nestedAttribute('realEstateImages');
        })
    });
}]).controller('RealEstateSearchCtrl', function ($rootScope, $scope, $routeParams, RealEstate) {
    $rootScope.titlePage = "Manager - Pesquisa de Imóveis";
    BaseCrudCtrl.call(this, $scope, $routeParams, RealEstate);

    var initComponents = function () {
        $('[rel=panels]').panels();
        $('#startPrice, #endPrice').maskMoney();
    };

    initComponents();

    $scope.resetSearchForm = function () {
        $scope.searchParams = {
            code: null
            , keyWord: null
        };
        angular.element('#code').focus();
    };

    $scope.resetSearchForm();

    $scope.search();

}).controller('RealEstateCtrl', function ($rootScope, $scope, $http, $routeParams, RealEstate) {
    $rootScope.titlePage = "Manager - Gestão de Imóveis";
    BaseCrudCtrl.call(this, $scope, $routeParams, RealEstate);

    $scope.resetSaveForm = function (form) {
        if (form) form.$setPristine();
        $scope.entity = {
            apartmentComplex: false
            , barbecueGrill: false
            , concierge: false
            , elevator: false
            , fireplace: false
            , playGround: false
            , rentType: 'apartment'
            , conservation: 'perfect'
            , furnished: 'no'
        };
        $('#title').focus();
    };

    var __map = null;
    var __marker = null;

    $scope.redrawMap = function() {
        setTimeout(function(){
            google.maps.event.trigger(__map, 'resize');
            if($scope.entity.id) {
                $scope.afterLoadEntity();
            }
        }, 200);
    };

    var initComponents = function () {

        $('[rel=panels]').panels();

        var searchField = $('#neighborhoodText');

        searchField.autocomplete({
            source: function (request, response) {
                var val = searchField.val().trim();
                searchField.addClass('ui-autocomplete-loading');
                $http.get("/locations/search-neighborhoods/" + val).success(function (data) {
                    searchField.removeClass('ui-autocomplete-loading');
                    response($.map(data, function (item) {
                        return {
                            label: item.name + ' - ' + item.city.name + '/' + item.city.state.name,
                            value: item.name + ' - ' + item.city.name + '/' + item.city.state.name,
                            id: item.id
                        }
                    }));
                }).error(function (data, status, headers, config) {
                    AppToast.showDanger(AppStorage.getUser().name, 'Aconteceu algum problema na tentativa de consultar bairros no sistema. Por favor contate o suporte.');

                    response($.map([], function (item) {
                        return {
                            label: item.name,
                            value: item.name,
                            id: item.id
                        }
                    }));
                });
            }
            , select: function (event, ui) {
                $scope.entity.neighborhoodId = ui.item.id;
            }
        });

        var searchPlace = $("#placeSearch");
        var geocoder = new google.maps.Geocoder();
        searchPlace.autocomplete({
            source : function(request, response) {
                searchPlace.addClass('ui-autocomplete-loading');
                geocoder.geocode( { 'address': searchPlace.val()}, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        searchPlace.removeClass('ui-autocomplete-loading');
                        response($.map(results, function(item) {
                            return {
                                label : item.formatted_address,
                                value : '',
                                data : item
                            }
                        }));
                    }
                });
            },
            minLength : 3,
            select : function(event, ui) {
                __map.setZoom(16);
                __map.panTo(ui.item.data.geometry.location);
            }
        });

        var myOptions = {
            zoom: 10,
            draggableCursor: 'crosshair',
            center: new google.maps.LatLng(-30.032646, -51.211120),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        __map = new google.maps.Map(document.getElementById("divGmap"), myOptions);

        google.maps.event.addListener(__map, 'click', function (e) {
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({
                latLng: e.latLng
            }, function (responses) {
                if (responses && responses.length > 0) {
                    var st = AppMaps.getValueByType('route', responses).longName;
                    $scope.entity.latitude = e.latLng.lat();
                    $scope.entity.longitude = e.latLng.lng();
                    $scope.entity.street = st;
                    $scope.entity.neighborhood = AppMaps.getValueByType('neighborhood', responses).longName;
                    $scope.city = AppMaps.getValueByType('locality', responses).longName;
                    $scope.$apply();

                    $('#states option:contains(' + AppMaps.getValueByType('administrative_area_level_1', responses).longName + '):first').prop('selected', true).trigger('change');

                    if (__marker) __marker.setMap(null);

                    __marker = new google.maps.Marker({
                        position: e.latLng,
                        map: __map,
                        title: st
                    });

                } else {
                    AppToast.showWarn('Falha', 'Não foi possível comunicar com os servidores do google. Aguarde um momento e tente novamente.');
                }
            });
        });

        $('#filesToUpload').on('change', function (event) {
            fileSelect(event);
        });
    };

    $scope.searchPostalCode = function (e) {
        if ((e.keyCode > 95 && e.keyCode < 106) && /\d{8}/gi.test($scope.entity.postalCode)) {
            angular.element('#postalCode').addClass('ui-autocomplete-loading');
            $http.get('/locations/search-postal-code/' + $scope.entity.postalCode).success(function (data, status, headers, config) {
                angular.element('#postalCode').removeClass('ui-autocomplete-loading');
                $scope.entity.street = (data.addressType != '' ? data.addressType + ' ' : '') + data.address;
                angular.element("#neighborhoodText").val(data.neighborhood.name + ' - ' + data.neighborhood.city.name + '/' + data.neighborhood.city.state.name);
                $scope.entity.neighborhoodId = data.neighborhood.id

            }).error(function (data, status, headers, config) {
                angular.element('#postalCode').removeClass('ui-autocomplete-loading');
                if (status == 404) {
                    AppToast.showInfo(AppStorage.getUser().name, "Não encontramos o CEP " + $scope.entity.postalCode);
                } else {
                    AppToast.showDanger(AppStorage.getUser().name, HttpMsgs.getMsg(status));
                }

            });
        }
    };

    $scope.deletePic = function (key, idx) {
        BootstrapDialog.confirm(AppStorage.getUser().name + ', tem certeza que deseja remover essa foto?', function (result) {
            if (result) {
                $http.delete('/manager/real_estates/delete-pic/' + key).success(function () {
                    $scope.entity.realEstateImages.splice(idx, 1);
                    $scope.$apply();
                    AppToast.showInfo(AppStorage.getUser().name, 'Foto removida com sucesso');
                }).error(function (data, status) {
                    if (status == 406) {
                        AppToast.showDanger(AppStorage.getUser().name, 'Você não possui permissão para executar essa ação.');
                    } else {
                        AppToast.showDanger(AppStorage.getUser().name, 'Aconteceu algum problema ao tentar excluir a foto. Recarregue a página clicando <a href="javascript:location.reload()">aqui</a> e se o problema persistir, contate a administração.');
                    }
                });
            }
        });
    };


    $scope.zoomPic = function (url) {
        $('#fotoZoomModal').modal('show');
        $('#imgZoom').attr('src', url);
    };

    var createProgress = function (id) {
        $('#progressArea').append('<div class="progress progress-striped active" id="' + id + '"><div class="progress-bar progress-bar-info" role="progressbar" aria-valuemax="100" style="width: 0"></div></div>');
    };

    function destroyProgress(id) {
        $('#' + id).fadeOut(1000, function () {
            $(this).remove();
        });
    }

    var fileSelect = function (evt) {
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            var files = evt.target.files;

            for (var i = 0; file = files[i]; i++) {
                if (!file.type.match('image.*')) continue;

                createFile(file, i);
            }
        } else {
            alert('Seu browser não suporta algumas funcionalidades necessárias. Utilize uma versão mais atualizada.');
        }
    };

    var createFile = function (file, i) {
        var reader = new FileReader();
        reader.file = file;
        reader.onloadend = function () {
            var tempImg = new Image();
            tempImg.src = reader.result;
            tempImg.onload = function () {

                var fd = new FormData();
                fd.append('file', reader.file);
                fd.append('count', i);
                var fileName = reader.file.name;
                var idProgress = 'pgrs_' + i;

                fd.append('authenticity_token', $('meta[name=csrf-token]').attr('content'));
                fd.append('label', $scope.entity.title);
                sendFile(fd, idProgress, fileName);
            };
        };

        reader.readAsDataURL(file);
    };

    var sendFile = function (fd, idProgress, fileName) {
        $.ajax({
            url: '/manager/real_estates/upload-pic',
            data: fd,
            processData: false,
            contentType: false,
            type: 'POST', beforeSend: function () {
                createProgress(idProgress);
            }, progressUpload: function (evt) {
                var progr = $('#' + idProgress).find('div');
                if (evt.lengthComputable) {
                    var percentComplete = Math.round(evt.loaded * 100 / evt.total);
                    progr.css('width', percentComplete.toString() + '%');
                    progr.text(fileName + ' ' + percentComplete.toString() + '% enviado. Aguarde, estamos processando...');
                } else {
                    progr.text('');
                }
            }
        }).fail(function (d) {
            var progr = $('#' + idProgress).find('div');
            progr.removeClass('progress-bar-success').addClass('progress-bar-danger');
            progr.text('Ops... Falha no envio da imagem. Por favor, tente novamente...');
        }).done(function (d) {
            if ($scope.entity.realEstateImages === undefined) $scope.entity.realEstateImages = [];
            $scope.entity.realEstateImages.push(d);
            $scope.$apply();
        }).always(function () {
            setTimeout(function () {
                destroyProgress(idProgress);
            }, 1000);
        });
    };

    $scope.afterLoadEntity = function () {
        if (__map) {
            var latLng = new google.maps.LatLng(parseFloat($scope.entity.latitude), parseFloat($scope.entity.longitude));
            __marker = new google.maps.Marker({
                position: latLng,
                map: __map,
                title: $scope.entity.street
            });

            __map.setZoom(16);
            __map.panTo(latLng);
        }
        angular.element("#neighborhoodText").val($scope.entity.neighborhood.name + ' - ' + $scope.entity.neighborhood.city.name + '/' + $scope.entity.neighborhood.city.state.name);
    };

    initComponents();
    $scope.loadEntity();
    if ($routeParams.id === undefined) {
        $scope.resetSaveForm();
    } else {
        $('#title').focus();
    }
});
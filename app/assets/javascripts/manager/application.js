//= require manager/app-all-libs
//= require manager/mask-money/jquery.maskMoney.min
//= require manager/masked-input/jquery.maskedinput.min
//= require manager/angular/angular
//= require manager/angular/angular-animate
//= require manager/angular/angular-resource
//= require manager/angular/angular-route
//= require manager/angular/angular-rails-resource
//= require manager/angular/angular-application
//= require_directory .
//= require_self

/**
 * Created by Picanha on 15/01/2015.
 */
var AppToast = {
    showInfo : function(tlt, c) {
        $.growl.notice({ title: tlt,  message: c });
    }
    ,showWarn : function(tlt, c) {
        $.growl.warning({ title: tlt,  message: c });
    }
    ,showDanger : function(tlt, c) {
        $.growl.error({ title: tlt,  message: c });
    }
    ,showValidMsg : function(data) {
        this.showWarn(AppStorage.getUser().name, ValidMsgs.toLi(data));
    }
    ,showHttpMsg : function(tlt, error) {
        if(typeof(error) == 'object') {
            this.showDanger(tlt, HttpMsgs.getMsg(error.status));
        }
    }
    ,showUnknowMsg : function() {
        this.showWarn('Ops ' + AppStorage.getUser().name, 'Aconteceu algum problema desconhecido. Caso persista, entre em contato com o suporte.');
    }
};

var HttpMsgs = {
    getMsg : function(code) {
        switch (code) {
            case 200:
            case 201:
            case 202:
            case 203:
            case 204:
            case 205:
            case 206:
            case 207:
            case 226: return "Ok";
            case 300:
            case 301:
            case 302:
            case 303:
            case 304:
            case 305:
            case 306:
            case 307: return "Modificado temporariamente";
            case 400: return "Ops... Ocorreu algum erro ao tentar se comunicar com o servidor";
            case 401: return "Você não está autorizado a acessar esse recurso";
            case 402: return "Você precisa pagar para acessar esse recurso";
            case 403: return "Proibido acessar este recurso";
            case 404: return "Você tentou acessar um recurso inexistente";
            case 405:
            case 406:
            case 407:
            case 408:
            case 409:
            case 410:
            case 411:
            case 412:
            case 413:
            case 414:
            case 415:
            case 416:
            case 417:
            case 422:
            case 423:
            case 424:
            case 426: return "Você necessita atualizar seu aplicativo";
            case 500:
            case 501:
            case 502:
            case 503:
            case 504:
            case 505:
            case 507:
            case 510: return "Ocorreu um erro no servidor. Por favor tente mais tarde ou contate o suporte";
            default:
                return "Olá, não sei o que aconteceu. Não estou conseguindo comunicar com o servidor. Isso pode estar ocorrendo por não estar conectado na internet ou o servidor está passando por alguma manutenção.";

        }
    }
};

var ValidMsgs = {
    toLi : function (o) {
        var s = '<ul class="no-padding no-margin">';
        for(var k in o) s += '<li>' + o[k] + '</li>';
        return s + '</ul>';
    }
};

var AppStorage = {
    USER_KEY: 'USER_KEY'
    , put: function (key, val) {
        localStorage.setItem(key, val);
    }
    , get: function (key) {
        return localStorage.getItem(key);
    }
    , destroy: function (key) {
        localStorage.removeItem(key);
    }
    , getUser: function () {
        var o = jQuery.parseJSON(this.get(this.USER_KEY));
        return jQuery.extend({name: '', email: ''}, o)
    }
    , saveUser: function (obj) {
        this.put(this.USER_KEY, JSON.stringify(obj))
    }
};


var AppForm = {
    disableSaveBtn : function() {
        return $("button.btn-save").attr("disabled", "disabled");
    },
    enableSaveBtn : function() {
        return $("button.btn-save").removeAttr("disabled");
    },
    checkAll : function(selector) {
        $(selector).each(function(){$(this).iCheck('check'); return true;})
    },
    uncheckAll : function(selector) {
        $(selector).each(function(){$(this).iCheck('uncheck'); return true;})
    }
};

var AppMaps = {
    getValueByType : function(type, responses) {
        if (responses && responses.length > 0 && responses[0].address_components) {
            for(var i = 0; i < responses[0].address_components.length; i++) {
                if(AppMaps.checkType(type, responses[0].address_components[i].types)){
                    return {longName: responses[0].address_components[i].long_name, shortName: responses[0].address_components[i].short_name}
                }
            }

            return {longName: '', shortName: ''};
        }
    },
    checkType : function(type, types) {
        for(var i = 0; i < types.length; i++) {
            if(types[i] == type) return true;
        }

        return false;
    }
};


(function addXhrProgressEvent($) {
    var originalXhr = $.ajaxSettings.xhr;
    $.ajaxSetup({
        xhr: function() {
            var req = originalXhr(), that = this;
            if (req) {
                if (typeof req.addEventListener == "function" && that.progress !== undefined) {
                    req.addEventListener("progress", function(evt) {
                        that.progress(evt);
                    }, false);
                }
                if (typeof req.upload == "object" && that.progressUpload !== undefined) {
                    req.upload.addEventListener("progress", function(evt) {
                        that.progressUpload(evt);
                    }, false);
                }
            }
            return req;
        }
    });
})(jQuery);
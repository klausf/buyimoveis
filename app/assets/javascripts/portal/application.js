/**
 * Created by Picanha on 21/01/2015.
 */
//= require portal/bootstrap.js
//= require portal/gmap3.min
//= require portal/jquery.easing
//= require portal/jquery.jcarousel.min
//= require portal/imagesloaded.pkgd.min
//= require portal/masonry.pkgd.min
//= require portal/select2
//= require portal/select2_locale_pt-BR
//= require portal/jquery.nicescroll.min
//= require portal/script.js
//= require_self


function bycode() {
    var val = $('#code').val();
    if(val != '') document.location.href='/imovel/codigo/' + val;
}

$(document).ready(function(){
    $('#code').keypress(function(e) {
        if(e.which == 13)  bycode();
    });

    $('#ordenatorRealEstates').on('change', function(){
        $('#ordenationRealEstates').val($(this).val());

        $('#btnSearchRealEstate').trigger('click');
    });

    initSelectNeighborhoods($('#searchCity'));
    var types = [{text: 'Apartamento', id: 'apartment'}
    , {text: 'Casa', id: 'home'}
    , {text: 'Terreno', id: 'land'}
    , {text: 'Lançamento', id: 'release'}
    , {text: 'Loft', id: 'loft'}
    , {text: 'Studio', id: 'studio'}
    , {text: 'Loja', id: 'store_business'}
    , {text: 'Sala Comercial', id: 'room_store'}
    , {text: 'Prédio Comercial', id: 'business_center'}
    , {text: 'Depósito', id: 'warehouse'}
    , {text: 'Cobertura', id: 'coverage'}
    , {text: 'Duplex', id: 'duplex'}
    ];

    $('#searchTipo').select2({multiple : true, data: types, allowClear: true, placeholder: 'Selecione o tipo'});

    var qtds = [{text: '1', id: '1'}
        , {text: '2', id: '2'}
        , {text: '3', id: '3'}
        , {text: '4', id: '4'}
        , {text: '5', id: '5'}];
    $('#searchBedrooms, #searchBathrooms, #searchSpaceVacation').select2({multiple : true, data: qtds, placeholder: 'Selecione a quantidade'});

    setTimeout(loadSocial, 1000);
});


function subscribeMe(form) {
    var f = $(form);
    var btn = $('#btnSubscribe');
    var btnLbl = $('#btnSubscribeLabel');
    var txt = btnLbl.text();
    var msgsModal = $('#messages');

    if(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/gi.test(f.find('#emailNewsLetter').val())) {

        btnLbl.text('Aguarde...');
        btn.prop('disabled', true);
        $.ajax({
            type: "POST",
            url: f.attr('action'),
            data: f.serialize(),
            dataType: 'json'
        }).done(function(data) {
            msgsModal.modal('show').find('.modal-body').text(data.email.join(', '));
        }).fail(function() {
            msgsModal.modal('show').find('.modal-body').text('Ops... Aconteceu algum problema na tentativa de inscrever o e-mail. Por favor tente novamente e caso persista, entre em conato conosco.');
        }).always(function(){
            btn.prop('disabled', false);
            btnLbl.text(txt);
        });
    } else {
        msgsModal.modal('show').find('.modal-body').text('Esse é um e-mail inválido. Verifique e tente novamente.');
    }

    return false;
}

function contact(form) {
    var f = $(form);
    var btn = $('#btnContact');
    var btnLbl = $('#btnContactLabel');
    var txt = btnLbl.text();
    var msgsModal = $('#messages');

    if(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/gi.test(f.find('#email').val())) {

        btnLbl.text('Aguarde...');
        btn.prop('disabled', true);
        $.ajax({
            type: "POST",
            url: f.attr('action'),
            data: f.serialize(),
            dataType: 'json'
        }).done(function(data) {
            msgsModal.modal('show').find('.modal-body').text(data.message);
            f.find(':text,input[type=email],textarea').val('')
        }).fail(function() {
            msgsModal.modal('show').find('.modal-body').text('Ops... Aconteceu algum problema na tentativa de contatar a gente. Por favor tente novamente.');
        }).always(function(){
            btn.prop('disabled', false);
            btnLbl.text(txt);
        });
    } else {
        msgsModal.modal('show').find('.modal-body').text('Esse é um e-mail inválido. Verifique e tente novamente.');
    }
    return false;
}

function proposal(form) {
    var f = $(form);
    var btn = $('#btnProposal');
    var btnLbl = $('#btnProposalLabel');
    var txt = btnLbl.text();
    var msgsModal = $('#messages');

    if(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/gi.test(f.find('#email').val())) {

        btnLbl.text('Aguarde...');
        btn.prop('disabled', true);
        $.ajax({
            type: "POST",
            url: f.attr('action'),
            data: f.serialize(),
            dataType: 'json'
        }).done(function(data) {
            msgsModal.modal('show').find('.modal-body').text(data.message);
            f.find(':text,input[type=email],input[type=tel],textarea').val('')
        }).fail(function() {
            msgsModal.modal('show').find('.modal-body').text('Ops... Aconteceu algum problema na tentativa de contatar a gente. Por favor tente novamente.');
        }).always(function(){
            btn.prop('disabled', false);
            btnLbl.text(txt);
        });
    } else {
        msgsModal.modal('show').find('.modal-body').text('Esse é um e-mail inválido. Verifique e tente novamente.');
    }
    return false;
}

function initSelectNeighborhoods(obj) {
    var neigh = $("#searchNeighborhood");
    neigh.addClass('ui-autocomplete-loading');
    $.ajax({
        type: "GET",
        url: '/locations/search-neighborhoods-by-city/' + $(obj).val(),
        dataType: 'json'
    }).done(function(data) {
        var options = [];
        for(var i = 0; i < data.length; i++) {
            options.push({id : data[i].id, text : data[i].name});
        }

        neigh.select2({data: options, multiple: true, allowClear: true, placeholder: 'Selecione o bairro...'});
    }).fail(function() {
        alert('Erro')
    }).always(function(){
        neigh.removeClass('ui-autocomplete-loading');
    });
}
function clearParameters() {
    $('#minprice option:first, #maxprice option:first').attr('selected', true);
    $("#searchNeighborhood, #searchTipo, #searchBedrooms, #searchBathrooms, #searchSpaceVacation").val(null).trigger("change");
}

function loadSocial() {
    (function (d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.async = true;
        js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook­jssdk'));

    !function(d,s,id){
        var js, fjs=d.getElementsByTagName(s)[0];
        if(!d.getElementById(id)) {
            js=d.createElement(s);
            js.id=id;
            js.async = true;
            js.src="https://platform.twitter.com/widgets.js";
            fjs.parentNode.insertBefore(js,fjs);
        }
    }(document,"script","twitter­wjs");

    window.___gcfg={lang:'pt-BR'};
    (function(d, t){
        var po = d.createElement(t);
        po.type = 'text/javascript';
        po.async = true;
        po.src = 'https://apis.google.com/js/platform.js?publisherid=113943749530134856727';
        var s = d.getElementsByTagName(t)[0];
        s.parentNode.insertBefore(po, s);
    })(document, 'script');
}
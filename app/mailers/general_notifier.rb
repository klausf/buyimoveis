class GeneralNotifier < ActionMailer::Base
  helper :application
  default from: 'Contato Buy Imóveis <contato@buyimoveis.com.br>'

  def notify_subscribe_newsletter news_letter
    @news_letter = news_letter
    set_image_sign
    mail(:to => news_letter.email, :subject => 'Inscrição de recebimento de notícias!')
  end

  def notify_contact email, name, subject, text
    @data = {:email => email, :name => name, :subject => subject, :text => text}

    set_image_sign


    if Rails.env.production?
      mail(:to => 'contato@buyimoveis.com.br', :reply_to => email, :subject => 'Contato feito através do site!')
    else
      mail(:to => 'klausf@gmail.com', :reply_to => email, :subject => 'Contato feito através do site!')
    end
  end

  def notify_proposal_required email, name, phone, text, real_estate
    @real_estate = real_estate
    @data = {:email => email, :name => name, :phone => phone, :text => text}

    set_image_sign

    if Rails.env.production?
      mail(:to => 'contato@buyimoveis.com.br', :reply_to => email, :subject => 'Solicitação de Proposta!')
    else
      mail(:to => 'klausf@gmail.com', :reply_to => email, :subject => 'Solicitação de Proposta!')
    end


  end

  def send_proposal proposal, email_reply
    @proposal = proposal
    set_image_sign

    if Rails.env.production?
      mail(:to => proposal.email, :reply_to => email_reply, :subject => 'Solicitação de Proposta!')
    else
      mail(:to => 'klausf@gmail.com', :reply_to => email_reply, :subject => 'Solicitação de Proposta!')
    end
  end

  def forget_password user
    @user = user
    set_image_sign

    if Rails.env.production?
      mail(:to => user.email, :subject => 'Solicitação de Nova Senha!')
    else
      mail(:to => 'klausf@gmail.com', :subject => 'Solicitação de Nova Senha!')
    end
  end

  private

  def set_image_sign
    attachments.inline['logo-marca-buy-imoveis.jpg'] = File.read("#{Rails.root}/app/assets/images/logo-marca-buy-imoveis.jpg")
  end

end

class NeighborhoodsController < ApplicationController
  before_action :set_user_session
  before_action :require_authentication
  before_action :set_neighborhood, only: [:edit, :update, :destroy, :show]

  layout 'manager'

  # GET /neighborhoods
  def index
    respond_to do |f|
      f.html { render :layout => params[:l].nil? }
      f.json {

        @neighborhoods = Neighborhood.search(params[:key_word]).paginate(:page => params[:page], :per_page => 15).order('neighborhoods.name asc, cities.name asc, states.name asc')

        render :json => {
            :current_page => @neighborhoods.current_page,
            :per_page => @neighborhoods.per_page,
            :total_entries => @neighborhoods.total_entries,
            :entries => @neighborhoods.map{|n| {:id => n.id, :name => n.name, :cityId => n.city_id, :cityName => n.city.name, :stateName => n.city.state.name}}
        }
      }
    end

  end


  # GET /neighborhoods/1
  def show
    render :json => to_json(@neighborhood)
  end

  # GET /neighborhoods/new
  def new
    @neighborhood = Neighborhood.new
    render :layout => params[:l].nil?
  end

  # GET /neighborhoods/1/edit
  def edit
    respond_to do |f|
      f.html { render :layout => params[:l].nil? }
      f.json {
        set_neighborhood
        render :json => to_json(@neighborhood)
      }
    end
  end

  # POST /neighborhoods
  def create
    @neighborhood = Neighborhood.new(neighborhood_params)

    respond_to do |f|
      if @neighborhood.save
        f.html { redirect_to @neighborhood, notice: 'Bairro criado com sucesso.' }
        f.json { render :json => to_json(@neighborhood) }
      else
        f.html { render :new }
        f.json { render json: @neighborhood.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /neighborhoods/1
  def update
    respond_to do |f|
      if @neighborhood.update(neighborhood_params)
        f.html { redirect_to @neighborhood, notice: 'Bairro alterado com sucesso.' }
        f.json { render :show, status: :ok, location: @neighborhood }
      else
        f.html { render :edit }
        f.json { render json: @neighborhood.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /neighborhoods/1
  def destroy
    @neighborhood.destroy
    respond_to do |f|
      f.html { redirect_to neighborhoods_url, notice: 'Bairro removido com sucesso.' }
      f.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_neighborhood
      @neighborhood = Neighborhood.joins(:city => :state).find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def neighborhood_params
      params.require(:neighborhood).permit(:name, :city_id)
    end

  def to_json n
    {:id => n.id, :name => n.name, :city_id => n.city_id, :city => {:name => n.city.name, :state => {:id => n.city.state_id, :name => n.city.state.name, :acronym => n.city.state.acronym}}}
  end
end

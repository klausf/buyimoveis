class LookRealEstateController < ApplicationController
  def index
    set_cities_with_real_estate

    set_total_types_real_estate

    id = params[:id]

    if !id.nil? and !id.blank?
      @real_estate = RealEstate.joins(:neighborhood => :city).includes(:real_estate_images).find id
    end


    if @real_estate.nil?
      flash[:notice] = 'Não encontramos imóveis com os dados informados'
      redirect_to :controller => 'search', :action => :index
    else
      expires_in 5.minutes, public: true
    end
  end

  def by_code

    set_total_types_real_estate

    code = params[:code]

    if !code.nil? and !code.blank?
      @real_estate = RealEstate.includes(:real_estate_images).find code.gsub(/\D+/, '').to_i
    end


    if !@real_estate.nil?
      expires_in 5.minutes, public: true
      render :action => :index
    else
      flash[:notice] = 'Não encontramos imóvel como código informado.'
      redirect_to :controller => 'search', :action => :index
    end
  end

  def proposal
    @proposal = Proposal.where(:key => params[:key]).joins(:real_estate).first
  end

  def call_proposal
    real_state = RealEstate.find params[:real_estate_id]

    GeneralNotifier.notify_proposal_required(params[:email], params[:name], params[:phone], params[:message], real_state).deliver
    render :json => {:message => 'Solicitação feita com sucesso. Em breve enviaremos uma proposta.'}
  end
end

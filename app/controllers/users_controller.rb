class UsersController < ApplicationController
  before_action :require_authentication
  before_action :set_user_session
  before_action :require_admin_permission, :except => [:edit, :update, :show, :profile]
  before_action :set_user, only: [:edit, :update, :destroy, :show]
  layout 'manager'

  # GET /users
  def index
    respond_to do |f|
      f.html { render :layout => params[:l].nil? }
      f.json {
        @users = User.search(params[:key_word], params[:profile]).where('id <> ?', @user_session.id).paginate(:page => params[:page], :per_page => 15).order('id asc')
        render :json => {
                   :current_page => @users.current_page,
                   :per_page => @users.per_page,
                   :total_entries => @users.total_entries,
                   :entries => @users.map { |u| {:id => u.id, :name => u.name, :email => u.email, :profile => u.profile} }
               }
      }
    end
  end

  # GET /users/1
  def show
    render :json => {:id => @user.id, :name => @user.name, :email => @user.email, :profile => @user.profile, :new_pass => '', :new_pass_confirm => ''}
  end

  # GET /users/new
  def new
    @user = User.new
    render :layout => params[:l].nil?
  end

  # GET /users/1/edit
  def edit
    respond_to do |f|
      f.html { render :layout => params[:l].nil? }
      f.json {
        set_user
        render :json => @user
      }
    end
  end

  def profile
    @user = User.find @user_session.id

    respond_to do |f|
      f.html { render :action => :edit, :layout => params[:l].nil? }
      f.json { render :json => {:id => @user.id, :name => @user.name, :email => @user.email, :profile => @user.profile, :newPass => '', :newPassConfirm => ''} }
    end
  end

  # POST /users
  def create
    @user = User.new(user_params)

    respond_to do |f|
      if @user.save
        f.html { redirect_to @user, notice: 'Usuário criado com sucesso.' }
        f.json { render :json => {:id => @user.id, :name => @user.name, :email => @user.email, :profile => @user.profile} }
      else
        f.html { render :new }
        f.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  def update
    if @user_session.eql? @user
      result = @user.update_profile(user_params_me)
    else
      result = @user.update(user_params)
    end

    respond_to do |f|
      if result
        f.html { redirect_to @user, notice: 'Usuário alterado com sucesso.' }
        f.json { head :no_content }
      else
        f.html { render :edit }
        f.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  def destroy

    @user.update({:removed => true})

    respond_to do |f|
      f.html { redirect_to users_url, notice: 'Usuário removido com sucesso.' }
      f.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def user_params
    params.require(:user).permit(:name, :email, :profile)
  end

  def user_params_me
    params.require(:user).permit(:name, :email, :password, :new_pass, :new_pass_confirm)
  end
end

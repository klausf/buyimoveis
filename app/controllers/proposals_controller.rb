class ProposalsController < ApplicationController
  before_action :require_authentication
  before_action :set_user_session
  before_action :set_proposal, only: [:edit, :update, :destroy, :show, :send_proposal]

  layout 'manager'

  # GET /proposals
  def index
    respond_to do |f|
      f.html { render :layout => params[:l].nil? }
      f.json {

        @proposals = Proposal.search(params[:code], params[:real_estate_code], params[:key_word]).paginate(:page => params[:page], :per_page => 15).order('id desc')

        render :json => {
                   :current_page => @proposals.current_page,
                   :per_page => @proposals.per_page,
                   :total_entries => @proposals.total_entries,
                   :entries => @proposals.map{|p| {:id => p.id, :email => p.email, :code => p.code, :real_estate_id => p.real_estate_id, :realEstate => {:code => p.real_estate.code, :title => p.real_estate.title}}}
               }
      }
    end
  end

  def send_proposal
    GeneralNotifier.send_proposal(@proposal, @user_session.email).deliver
    render :json => {}
  end

  def real_estate
    re = RealEstate.find params[:id]
    render :json => {:id => re.id, :code => re.code, :notes => re.notes}
  end

  # GET /proposals/1
  def show
    render :json => {:id => @proposal.id, :code => @proposal.code, :email => @proposal.email, :notes => @proposal.notes, :real_estate => {:id => @proposal.real_estate_id, :code => @proposal.real_estate.code, :notes => @proposal.real_estate.notes}}
  end

  # GET /proposals/new
  def new
    render :layout => params[:l].nil?
  end

  # GET /proposals/1/edit
  def edit
    respond_to do |f|
      f.html { render :layout => params[:l].nil? }
      f.json {
        set_proposal
        render :json => @proposal
      }
    end
  end

  # POST /proposals
  def create
    @proposal = Proposal.new(proposal_params)
    @proposal.user_created_by_id = @user_session.id

    respond_to do |f|
      if @proposal.save
        f.html { redirect_to @proposal, notice: 'Proposta criada com sucesso.' }
        f.json { render :json => {:id => @proposal.id, :email => @proposal.email, :code => @proposal.code, :notes => @proposal.notes, :real_estate => {:id => @proposal.real_estate_id, :code => @proposal.real_estate.code, :notes => @proposal.real_estate.notes}} }
      else
        f.html { render :new }
        f.json { render json: @proposal.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /proposals/1
  def update
    respond_to do |f|
      if @proposal.update(proposal_params)
        f.html { redirect_to @proposal, notice: 'Proposta alterada com sucesso.' }
        f.json { head :no_content }
      else
        f.html { render :edit }
        f.json { render json: @proposal.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /proposals/1
  def destroy
    @proposal.update({:removed => true})
    respond_to do |f|
      f.html { redirect_to proposals_url, notice: 'Propostal removida com sucesso.' }
      f.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_proposal
    @proposal = Proposal.includes(:real_estate).find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def proposal_params
    params.require(:proposal).permit(:notes, :real_estate_id, :email)
  end
end

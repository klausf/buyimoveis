class NewsLettersController < ApplicationController
  before_action :set_user_session, :except => :add_me
  before_action :require_authentication, :except => :add_me
  before_action :set_news_letter, only: [:edit, :update, :destroy, :show]

  layout 'manager'

  # GET /news_letters
  def index
    respond_to do |f|
      f.html { render :layout => params[:l].nil? }
      f.json {

        @news_letters = NewsLetter.where('email like ?', "%#{params[:email]}%").paginate(:page => params[:page], :per_page => 15).order('id desc')

        render :json => {
                   :current_page => @news_letters.current_page,
                   :per_page => @news_letters.per_page,
                   :total_entries => @news_letters.total_entries,
                   :entries => @news_letters.map { |nl| {:id => nl.id, :email => nl.email, :createdAt => nl.created_at.strftime('%d/%m/%Y %H:%M')} }
               }
      }
    end
  end

  # GET /news_letters/1
  def show
    render :json => {:id => @news_letter.id, :email => @news_letter.email}
  end

  def subscribe
    @news_letter = NewsLetter.new(news_letter_params)

    respond_to do |f|
      if @news_letter.save
        GeneralNotifier.notify_subscribe_newsletter(@news_letter).deliver
        f.json {
          render :json => {:email => ['Inscrição feita com sucesso! Agora é só aguardar para receber novidades sobre imóveis...']}
        }
      else
        f.json { render :json => @news_letter.errors.to_json }
      end
    end
  end

  # GET /news_letters/new
  def new
    @news_letter = NewsLetter.new
    render :layout => params[:l].nil?
  end

  # GET /news_letters/1/edit
  def edit
    respond_to do |f|
      f.html { render :layout => params[:l].nil? }
      f.json {
        set_news_letter
        render :json => {:id => @news_letter.id, :email => @news_letter.email}
      }
    end
  end

  # POST /news_letters
  def create
    @news_letter = NewsLetter.new(news_letter_params)

    respond_to do |f|
      if @news_letter.save
        f.html { redirect_to @news_letter, notice: 'E-mail cadastrado com sucesso.' }
        f.json { render :json => {:id => @news_letter.id, :email => @news_letter.email} }
      else
        f.html { render :new }
        f.json { render json: @news_letter.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /news_letters/1
  def update
    respond_to do |f|
      if @news_letter.update(news_letter_params)
        f.html { redirect_to @news_letter, notice: 'E-mail alterado com sucesso.' }
        f.json { head :no_content }
      else
        f.html { render :edit }
        f.json { render json: @news_letter.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /news_letters/1
  def destroy
    @news_letter.destroy
    respond_to do |f|
      f.html { redirect_to news_letters_url, notice: 'E-mail removido com sucesso.' }
      f.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_news_letter
    @news_letter = NewsLetter.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def news_letter_params
    params.require(:news_letter).permit(:email)
  end
end

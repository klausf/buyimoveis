class ManagerHomeController < ApplicationController
  before_action :require_authentication
  before_action :set_user_session
  layout 'manager'

  include ActionView::Helpers::DateHelper

  def index
    respond_to do |f|
      f.html { render :layout => params[:l].nil? }
      f.json {
        current = DateTime.current

        real_estates = RealEstate.where('removed = ?', false).order('id desc')
        proposals = Proposal.where('removed = ?', false).order('id desc')

        render :json => {:realEstates => real_estates.map{|re|{:id => re.id, :title => re.title, :distance => distance_of_time_in_words(re.created_at, current)}}, :proposals => proposals.map{|p|{:id => p.id, :code => p.code, :distance => distance_of_time_in_words(p.created_at, current)}}}
      }
    end
  end
end
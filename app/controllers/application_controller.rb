class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :require_authentication, :only => [:load_states, :search_cities, :search_postal_code, :search_cities_by_name, :search_neighborhoods]


  def load_states
    states = State.select(:name, :id, :acronym, :updated_at).where(:country_id => 1).order('name asc')
    render :json => states.map {|s| {:name => s.name, :acronym => s.acronym, :id => s.id} }
  end

  def search_cities
    cities = City.select(:name, :id, :updated_at).where(:state_id => params[:state_id]).order('name asc')
    render :json => cities.map {|s| {:name => s.name, :id => s.id} }
  end

  def search_postal_code
    pc = params[:postal_code]

    if pc =~ /\d{8}/
      begin
        postal_code_info = BuscaEndereco.cep(pc)
        neighborhood = nil

        if !postal_code_info[:bairro].nil? and !postal_code_info[:bairro].blank?
          neighborhood = Neighborhood.joins(:city => :state).where('cities.name = ? and neighborhoods.name = ?', postal_code_info[:cidade], postal_code_info[:bairro]).first
        end

        if neighborhood.nil?
          neighborhood = Neighborhood.new
          neighborhood.city = City.joins(:state).where('cities.name = ?', postal_code_info[:cidade]).first unless postal_code_info[:cidade].nil?
          neighborhood.city ||= City.new
          neighborhood.city.state ||= State.new
          neighborhood.name = postal_code_info[:bairro]
        end

        expires_in 30.seconds, :public => true

        render :json => {:address => postal_code_info[:logradouro], :addressType => postal_code_info[:tipo_logradouro], :neighborhood => {:id => neighborhood.id, :name => neighborhood.name, :city => {:name => neighborhood.city.name, :state => {:name => neighborhood.city.state.name}}}}
      rescue
        render :json => {}, status: :not_found, content_type: 'application/json'
      end

    else
      render :json => {}, status: :ok, content_type: 'application/json'
    end
  end

  def search_cities_by_name
    cities = City.joins(:state).where('cities.name like ?', "%#{params[:q]}%").order('cities.name asc').limit 20
    render :json => cities.map {|s| {:name => s.name, :id => s.id, :state => {:name => s.state.name, :acronym => s.state.acronym}} }
  end

  def search_neighborhoods
    neighborhoods = Neighborhood.search(params[:q]).order('neighborhoods.name asc').limit 15

    expires_in 30.seconds, :public => true

    render :json => neighborhoods.map {|n| {:name => n.name, :id => n.id, :city => {:id => n.city_id, :name => n.city.name, :state => {:id => n.city.state_id, :name => n.city.state.name, :acronym => n.city.state.acronym}}}}
  end

  def search_neighborhoods_by_city

    expires_in 5.minutes, :public => true

    neighborhoods = Neighborhood.search_by_city params[:city_id]
    render :json => neighborhoods.map {|n| {:name => n.name, :id => n.id}}
  end

  protected
  def set_user_session
    if @user_session.nil? and session[:id]
      @user_session = User.find session[:id]
    end
  end


  def set_total_types_real_estate
    @real_estates_total_types = RealEstate.total_by_type
  end

  def set_last_real_estates limit
    @last_real_estates = RealEstate.last_added limit
  end

  def set_cities_with_real_estate
    @cities_with_real_estate = City.with_real_estate.order('name asc').limit 20
  end

  def require_admin_permission
    unless @user_session.admin?
      show_403
    end
  end

  def show_403
    respond_to do |f|
      f.html {
        if params[:l].nil?
          redirect_to '/403'
        else
          redirect_to '/templates/403'
        end
      }

      f.json { render json: {:error => 'Você não tem permissão para executar essa ação!'}, status: 403 }
    end
  end

  def require_authentication
    if session[:id].nil?
      redirect_to '/manager/session'
    end
  end
end

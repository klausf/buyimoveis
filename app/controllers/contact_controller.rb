class ContactController < ApplicationController
  include ActionView::Helpers::TextHelper

  def index

  end

  def contact_us
    GeneralNotifier.notify_contact(params[:email], params[:name], params[:subject], simple_format(params[:message])).deliver
    render :json => {:message => 'Contato feito com sucesso. Em breve responderemos sua mensagem.'}
  end
end

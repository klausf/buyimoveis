class SearchController < ApplicationController
  def index
    set_cities_with_real_estate

    set_total_types_real_estate

    neighborhoods = params[:bairros]
    type = params[:tipos]
    min_price = params[:precoMinimo]
    max_price = params[:precoMaximo]
    bedrooms = params[:dormitorios]
    bathrooms = params[:suites]
    space_vacation = params[:vagasGaragem]


    @real_estates = RealEstate.search_portal(neighborhoods, type, min_price, max_price, bedrooms, bathrooms, space_vacation).paginate(:page => params[:page], :per_page => 9).order(ordenacao(params[:ordenacao]))

  end

  def type
    set_cities_with_real_estate
  end

  private
  def ordenacao order
    if order.nil? or order.blank? or 'mais-recentes'.eql?(order)
      'real_estates.id desc'
    elsif 'menor-preco'.eql?(order)
      'real_estates.price asc'
    elsif 'maior-preco'.eql?(order)
      'real_estates.price desc'
    else
      'real_estates.id desc'
    end
  end
end

class HomeController < ApplicationController
  def index
    set_cities_with_real_estate

    set_last_real_estates 4
  end

  def sitemap

  end
end

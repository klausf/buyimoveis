class CitiesController < ApplicationController
  before_action :set_user_session
  before_action :require_authentication
  before_action :set_city, only: [:edit, :update, :destroy, :show]
  layout 'manager'

  # GET /cities
  def index
    respond_to do |f|
      f.html { render :layout => params[:l].nil? }
      f.json {

        @cities = City.search(params[:key_word]).paginate(:page => params[:page], :per_page => 15).order('cities.name asc, states.name asc')

        render :json => {
                   :current_page => @cities.current_page,
                   :per_page => @cities.per_page,
                   :total_entries => @cities.total_entries,
                   :entries => @cities.map{|c| to_j(c)}
               }
      }
    end

  end


  # GET /cities/1
  def show
    render :json => to_j(@city)
  end

  # GET /cities/new
  def new
    @city = City.new
    render :layout => params[:l].nil?
  end

  # GET /cities/1/edit
  def edit
    respond_to do |f|
      f.html { render :layout => params[:l].nil? }
      f.json {
        set_city
        render :json => to_j(@city)
      }
    end
  end

  # POST /cities
  def create
    @city = City.new(city_params)

    respond_to do |f|
      if @city.save
        f.html { redirect_to @city, notice: 'Cidade criada com sucesso.' }
        f.json { render :json => to_j(@city) }
      else
        f.html { render :new }
        f.json { render json: @city.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cities/1
  def update
    respond_to do |f|
      if @city.update(city_params)
        f.html { redirect_to @city, notice: 'Cidade alterada com sucesso.' }
        f.json { render :json => to_j(@city)}
      else
        f.html { render :edit }
        f.json { render json: @city.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cities/1
  def destroy
    @city.destroy
    respond_to do |f|
      f.html { redirect_to cities_url, notice: 'Cidade removida com sucesso.' }
      f.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_city
    @city = City.joins(:state).find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def city_params
    params.require(:city).permit(:name, :state_id)
  end

  def to_j c
    {:id => c.id, :name => c.name, :state_id => c.state_id, :state => {:id => c.state_id, :name => c.state.name, :acronym => c.state.acronym}}
  end

end

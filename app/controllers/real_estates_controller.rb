class RealEstatesController < ApplicationController
  before_action :require_authentication
  before_action :set_user_session
  before_action :require_admin_permission, only: [:update, :destroy, :create, :delete_pic, :upload_pic]
  before_action :set_real_estate, only: [:edit, :show, :update, :destroy]
  after_action :expire_caches, only: [:update, :create, :destroy, :delete_pic, :upload_pic]

  layout 'manager'

  include ActionView::Helpers::NumberHelper
  include ApplicationHelper

  # S3_ACCESS_KEY_ID = 'AKIAIE7MET2SKDNYZRSQ'
  # S3_ACCESS_KEY = 'dh/LIeFpo8oKLhRh+iVpJMu6KrkY331EiQal3Aul'
  # BUY_IMOVEIS_BUCKET = 'buy-imoveis-imgs'

  S3_ACCESS_KEY_ID = 'AKIAIUEILIZQ3AZYHQAA'
  S3_ACCESS_KEY = 'L6MySa2KL3XUKiqJK6nttaVNyA0Wxld1jbQ3irwE'
  BUY_IMOVEIS_BUCKET = 'buyimoveis-imgs'

  # GET /real_estates
  def index

    respond_to do |f|
      f.html { render :layout => params[:l].nil? }
      f.json {
        @real_estates = RealEstate.search(params[:code], params[:key_word], params[:start_price].to_s.gsub('.', '').gsub(',', '.'), params[:end_price].to_s.gsub('.', '').gsub(',', '.')).paginate(:page => params[:page], :per_page => 15).order('real_estates.updated_at desc, real_estates.id desc')

        render 'index'
      }
    end
  end


  def upload_pic
    s3 = AWS::S3.new(:access_key_id => S3_ACCESS_KEY_ID, :secret_access_key => S3_ACCESS_KEY)
    key = params[:file].original_filename
    label = params[:label]

    if /(.*)\.(.*)$/ =~ key

      sec = SecureRandom.hex(4)
      if label.nil? or label.blank?
        key = "#{sanitize_url($1)}-#{sec}.#{$2}"
      else
        key = "#{sanitize_url(label)}-#{sec}.#{$2}"
      end
    end

    s3.buckets[BUY_IMOVEIS_BUCKET].objects[key].write(:file => params[:file], :acl => :public_read, :cache_control => 'Public, max-age=315360000', :expires => (Time.now + 365.days).httpdate)


    render :json => {:key => key, :label => label, :url => s3.buckets[BUY_IMOVEIS_BUCKET].objects[key].public_url.to_s}
  end

  def delete_pic
    s3 = AWS::S3.new(:access_key_id => S3_ACCESS_KEY_ID, :secret_access_key => S3_ACCESS_KEY)
    key = "#{params[:key]}.#{params[:format]}"
    s3.buckets[BUY_IMOVEIS_BUCKET].objects[key].delete

    RealEstateImage.delete_all(['url like ?', "%#{key}"])

    render :json => {}
  end

  # GET /real_estates/1
  def show
    render 'show'
  end

  # GET /real_estates/new
  def new
    @real_estate = RealEstate.new
    render :layout => params[:l].nil?
  end

  # GET /real_estates/1/edit
  def edit
    respond_to do |f|
      f.html { render :layout => params[:l].nil? }
      f.json {
        set_real_estate
        render 'show'
      }
    end
  end

  # POST /real_estates
  def create
    @real_estate = RealEstate.new(real_estate_params)
    @real_estate.user_created_by_id = @user_session.id

    respond_to do |f|
      if @real_estate.save
        f.html { redirect_to @real_estate, notice: 'Imóvel criado com sucesso.' }
        f.json { render 'show' }
      else
        f.html { render :new }
        f.json { render json: @real_estate.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /real_estates/1
  def update
    @real_estate.user_changed_by_id = @user_session.id

    respond_to do |f|
      if @real_estate.update(real_estate_params)
        f.html { redirect_to @real_estate, notice: 'Imóvel alterado com sucesso.' }
        f.json { render 'show' }
      else
        f.html { render :edit }
        f.json { render json: @real_estate.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /real_estates/1
  def destroy
    @real_estate.update({:removed => true, :user_changed_by_id => @user_session.id})

    respond_to do |f|
      f.html { redirect_to customers_url, notice: 'Imóvel removido com sucesso.' }
      f.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_real_estate
    @real_estate = RealEstate.includes(:real_estate_images).joins(:neighborhood => [:city => :state]).find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def real_estate_params
    p = params.require(:real_estate).permit(:title, :rent_type, :latitude, :longitude, :postal_code, :neighborhood_id, :street,
                                            :street_number, :street_notes, :apartment_complex, :barbecue_grill, :concierge, :elevator, :fireplace, :planned_neighborhood,
                                            :suite, :lavatory, :vacancy, :hot_water, :central_gas, :barbecue_condo, :anchor_to_split,
                                            :anchor_to_hood, :forest, :intercom, :individual_measurement_gas, :individual_measurement_water, :vacancy_guest,
                                            :port_cochare, :party_room, :terrace, :room_convention, :gourmet_space, :adult_pool, :children_pool, :gym,
                                            :pantry, :dressing, :dependence_maid, :tv_circuit, :automatization, :generator,
                                            :garth, :balcony, :service_area, :caretaker, :whirlpool, :lobby,
                                            :playground, :price, :apartment_complex_price, :total_area, :area_usable, :bathrooms,
                                            :solar_north, :solar_south, :solar_east, :solar_west, :position_back, :position_front, :negotiable, :busy, :exclusivity, :construction_company,
                                            :ground_type, :service, :inside_description, :owner_name, :owner_rg, :owner_cpf, :owner_phone, :owner_email, :bookie_name,
                                            :bookie_email, :bookie_phone, :bookie_where_keys, :square_meter_value, :brokerage_value, :iptu_value, :front_meters,
                                            :back_meters, :construtive_index, :enrollment_number, :enrollment_vacation_number, :registration_zone, :elevators, :rooms,
                                            :balconies, :kitchens, :year_construction, :conservation,
                                            :furnished, :towers_count, :apartment_floor_count,
                                            :floor_count, :apartment_count, :vacancy_covered, :vacancy_uncovered, :vacancy_double, :vacancy_simple,
                                            :bedrooms, :parking_spaces, :notes, real_estate_images_attributes: [:id, :url, :label])
    p[:price] = p[:price].to_s.gsub('.', '').gsub(',', '.')
    p[:apartment_complex_price] = p[:apartment_complex_price].to_s.gsub('.', '').gsub(',', '.')
    p[:square_meter_value] = p[:square_meter_value].to_s.gsub('.', '').gsub(',', '.')
    p[:brokerage_value] = p[:brokerage_value].to_s.gsub('.', '').gsub(',', '.')
    p[:iptu_value] = p[:iptu_value].to_s.gsub('.', '').gsub(',', '.')

    p
  end

  def expire_caches
    expire_fragment('general_real_estates')
    expire_fragment('general_real_estates_home')
  end
end

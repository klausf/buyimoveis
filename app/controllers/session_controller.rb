class SessionController < ApplicationController
  layout false

  def index
  end

  def create
    @user = User.where('lower(email) = ? and removed = ?', params[:email].downcase, false).first
    if !@user.nil? and !@user.authenticate(params[:password]).nil?
      session[:id] = @user.id
      render :json => {:name => @user.name, :email => @user.email, id: @user.id}, status: :ok
    else
      render status: :unauthorized
    end
  end

  def destroy
    session[:id] = nil
    redirect_to action: :index
  end

  def forget_password
    @user = User.new(user_params)

    if @user.forget_password
      render :json => {:email => @user.email}, status: :ok
    else
      render json: @user.errors, status: :unauthorized
    end
  end

  private
  def user_params
    params.require(:user).permit(:email)
  end
end

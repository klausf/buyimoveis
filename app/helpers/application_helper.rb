module ApplicationHelper


  def active_menu ctrl_current, ctrl
    ctrl_current.eql?(ctrl) ? 'active' : ''
  end

  def touch_device?
    user_agent = request.headers['HTTP_USER_AGENT']
    user_agent.present? && user_agent =~ /\b(Android|iPhone|iPad|Windows Phone|Opera Mobi|Kindle|BackBerry|PlayBook)\b/i
  end

  def randomized_city_image
    images = Dir.glob("#{Rails.root.join('app/assets/images/cidades').to_path}/*").map{|f| {:name => File.basename(f)}}
    "cidades/#{images[rand(images.size)][:name]}"
  end


  def url_real_estate_code code
    "/imovel/codigo/#{code}"
  end

  def url_real_estate_full re
    "/imovel/#{re.id}/#{re.neighborhood.city.state.acronym.downcase}/#{sanitize_url(re.neighborhood.city.name)}/#{sanitize_url(re.title)}"
  end

  def city
    'Porto Alegre'
  end

  def neighborhood
    'Moinhos de Vento'
  end

  def street
    'Marques do Herval, 169/01'
  end

  def phone
    '5130243098'
  end

  def phone2
    '5189266854'
  end

  def phone_formatted(p)
    if /(\d{2})(\d{4})(\d+)/ =~ p
      return "(#{$1}) #{$2}-#{$3}"
    end
    ''
  end

  def sanitize_url url
    remove_acentos(url).gsub(/[^\w\/]|[!\(\)\.]+/, ' ').strip.downcase.gsub(/\ +/, '-')
  end

  def remove_acentos text
    return text if text.blank?

    text = text.gsub(/(á|à|ã|â|ä)/, 'a').gsub(/(é|è|ê|ë)/, 'e').gsub(/(í|ì|î|ï)/, 'i').gsub(/(ó|ò|õ|ô|ö)/, 'o').gsub(/(ú|ù|û|ü)/, 'u')
    text = text.gsub(/(Á|À|Ã|Â|Ä)/, 'A').gsub(/(É|È|Ê|Ë)/, 'E').gsub(/(Í|Ì|Î|Ï)/, 'I').gsub(/(Ó|Ò|Õ|Ô|Ö)/, 'O').gsub(/(Ú|Ù|Û|Ü)/, 'U')
    text = text.gsub(/ñ/, 'n').gsub(/Ñ/, 'N')
    text.gsub(/ç/, 'c').gsub(/Ç/, 'C')
  end

  def msg_good
    hour = Time.zone.now.hour

    if hour > 12 && hour < 18
      'Boa tarde'
    elsif hour > 18 || hour < 6
      return 'Boa noite'
    else
      'Bom dia'
    end
  end
end

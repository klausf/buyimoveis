json.key_format! camelize: :lower
json.extract! @real_estate, :rent_type, :furnished, :conservation

@real_estate.attributes.each do |k, v|

  if k =~ /^(created|updated).*/
    unless v.nil?
      json.set! k, v.to_datetime.strftime('%d/%m/%Y %R')
    end
  elsif k =~ /.*(price|value).*/
    unless v.nil?
      json.set! k, number_to_currency(v, :unit => '', :separator => ',', :delimiter => '.')
    end
  elsif k =~ /.*(phone).*/
    unless v.nil?
      json.set! k, "(#{v[0..1]}) #{v[2..5]}-#{v[6..15]}"
    end
  elsif k =~ /.*(cpf).*/
    unless v.nil?
      json.set! k, "#{v[0..2]}.#{v[3..5]}.#{v[6..8]}-#{v[9..11]}"
    end
  elsif k !~ /rent_type|furnished|conservation/
    json.set! k, v
  end
end

json.neighborhood do
  json.name @real_estate.neighborhood.name
  json.city do
    json.name @real_estate.neighborhood.city.name

    json.state do
      json.name @real_estate.neighborhood.city.state.name
      json.acronym @real_estate.neighborhood.city.state.acronym
    end
  end
end unless @real_estate.neighborhood.nil?

json.real_estate_images @real_estate.real_estate_images.each do |rei|
  json.extract! rei, :id, :label, :url
  json.key rei.url.gsub(/.*\/(.*)$/, '\1')
end

json.user_changed_by do
  unless @real_estate.user_changed_by.nil?
    json.extract! @real_estate.user_changed_by, :id, :name
  end
end
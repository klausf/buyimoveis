json.key_format! camelize: :lower
json.current_page @real_estates.current_page
json.per_page @real_estates.per_page
json.total_entries @real_estates.total_entries

json.entries @real_estates.each do |re|
  json.extract! re, :id, :title, :code
  json.neighborhood_name re.neighborhood.name
  json.city_name re.neighborhood.city.name
  json.price number_to_currency(re.price, :unit => '', :separator => ',', :delimiter => '.')
  json.type I18n.t("real_estate_type_#{re.rent_type}")
end

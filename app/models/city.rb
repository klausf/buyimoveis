class City < ActiveRecord::Base
  belongs_to :state
  has_many :neighborhoods

  validates_presence_of :name, :state_id

  scope :with_real_estate, ->  {
    where('exists (select 1 from real_estates re, neighborhoods n where re.neighborhood_id = n.id and n.city_id = cities.id and re.removed = ? and exists (select 1 from real_estate_images rei where rei.real_estate_id = re.id))', false)
  }


  scope :search, -> (text) {
    w = joins(:state)

    if /,/ =~ text
      text = text.split(',')

      w = w.where('cities.name like ?', "#{text[0].strip}%")
      w = w.where('states.name like ?', "#{text[1].strip}%") if text.length == 2

    else
      unless text.nil? or text.blank?
        w = w.where('cities.name like :text or states.name like :text', {:text => "%#{text.strip}%"})
      end
    end

    w
  }

end
class Neighborhood < ActiveRecord::Base
  belongs_to :city
  has_many :real_estates

  validates_presence_of :name, :city_id


  scope :search, -> (text) {
    w = joins(:city => :state)

    if /,/ =~ text
      text = text.split(',')

      w = w.where('neighborhoods.name like ?', "#{text[0].strip}%")
      w = w.where('cities.name like ?', "#{text[1].strip}%") if text.length >= 2
      w = w.where('states.name like ?', "#{text[2].strip}%") if text.length == 3

    else
      unless text.nil? or text.blank?
        w = w.where('neighborhoods.name like :text or cities.name like :text or states.name like :text', {:text => "%#{text.strip}%"})
      end
    end

    w
  }

  scope :search_by_city, -> (city_id) {
    joins(:city => :state).where('neighborhoods.city_id = ? and exists (select 1 from real_estates re where re.neighborhood_id = neighborhoods.id and re.removed = ? and exists (select 1 from real_estate_images rei where rei.real_estate_id = re.id))', city_id, false).order('neighborhoods.name asc')
  }

end

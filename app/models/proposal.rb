class Proposal < ActiveRecord::Base
  belongs_to :real_estate

  before_create :generate_number


  validates :email, format: {:with => /\A[^@]+@([^@\.]+\.)+[^@\.]+\z/}


  scope :search, -> (code, real_estate_code, text) {
    w = joins(:real_estate).where('proposals.removed = ?', false)


    if (code.nil? || code.blank?) and (real_estate_code.nil? or real_estate_code.blank?)
      unless text.nil? or text.blank?
        w = w.where('notes like :text', {:text => "%#{text}%"})
      end
    else
      if !code.nil? and !code.blank?
        w = w.where('proposals.code = ?', code)
      else
        w = w.where('real_estates.code = ?', real_estate_code)
      end
    end

    w
  }


  protected
  def generate_number
    self.key = SecureRandom.hex

    last = Proposal.select('code, created_at').where('id = (select max(id) from proposals)').lock(true).first
    current_date = Date.current

    if last.nil? || current_date.year > last.created_at.year
      self.code = "#{current_date.strftime('%Y%m') + '00001'}".to_i
    else
      self.code = last.code.to_i + 1
    end
  end
end
class RealEstateImage < ActiveRecord::Base
  belongs_to :real_estate, touch: true
  validates_presence_of :url
end
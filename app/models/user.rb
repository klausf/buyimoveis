require 'digest/sha2'
require 'securerandom'
class User < ActiveRecord::Base

  before_create :set_password

  attr_accessor :uncrypted_pass, :new_pass, :new_pass_confirm

  enum profile: {:admin => 0, :seller => 1}

  validates :name, length: {minimum: 2, maximum: 100}
  validates :email, format: {:with => /\A[^@]+@([^@\.]+\.)+[^@\.]+\z/}, uniqueness: true

  def authenticate pass
    if get_digest(pass, self.password).eql? self.password_digest
      return self
    end
    nil
  end

  scope :search, -> (text, profile) {
    w = where('removed = ?', false)

    unless text.nil? or text.blank?
      w = w.where('name like :text or email like :text', {:text => "%#{text}%"})
    end

    unless profile.nil? or profile.blank?
      w = w.where(:profile => profiles[profile])
    end

    w
  }

  def update_profile(attr)

    if authenticate(attr[:password]).nil?
      errors.add(:password, 'Senha inválida.')
    else

      if !attr[:new_pass].blank? and !attr[:new_pass_confirm].blank?
        if !attr[:new_pass].eql?(attr[:new_pass_confirm])
          errors.add(:new_pass_confirm, 'Nova senha e confirmação de nova senha não conferem. Verifique e tente novamente.')
          return false
        else
          attr[:password] = get_salt
          attr[:password_digest] = get_digest attr[:new_pass], attr[:password]
        end
        return update(attr.except!(:new_pass, :new_pass_confirm))
      else
        return update(attr.except!(:password))
      end
    end

    false
  end

  def forget_password
    user = User.where('lower(email) = ?', self.email.downcase).first

    Rails.logger.debug '####CHEGUEI AQUI'

    if user.nil?
      errors.add(:email, "E-mail #{email} não encontrado.")
    else
      set_password

      if user.update({:password => self.password, :password_digest => self.password_digest})
        user.uncrypted_pass = self.uncrypted_pass
        GeneralNotifier.forget_password(user).deliver
      else
        errors.add(:email, 'Falha na atualização da senha. Por favor tente novamente mais tarde.')
      end
    end

    errors.empty?
  end

  private
  def set_password
    self.uncrypted_pass = self.password_digest || SecureRandom.urlsafe_base64(6).downcase
    self.password = get_salt
    self.password_digest = get_digest self.uncrypted_pass, self.password
  end

  def get_digest(pass, salt)
    Digest::SHA2.hexdigest("--#{pass}-#{salt}--")
  end

  def get_salt
    Digest::SHA2.hexdigest(SecureRandom.hex(8))
  end
end

class NewsLetter < ActiveRecord::Base
  validates :email, format: {:with => /\A[^@]+@([^@\.]+\.)+[^@\.]+\z/}, uniqueness: true
end
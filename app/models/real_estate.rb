class RealEstate < ActiveRecord::Base
  before_save :clear_not_number
  before_validation :clear_not_number

  belongs_to :neighborhood
  belongs_to :user_changed_by, :class_name => 'User'

  has_many :real_estate_images
  accepts_nested_attributes_for :real_estate_images, :reject_if => lambda { |a| a[:url].blank? }

  enum rent_type: {:apartment => 0, :home => 1, :land => 2, :release => 3, :loft => 4, :studio => 5, :store_business => 6,  :room_store => 7, :business_center => 8, :warehouse => 9, :coverage => 10, :duplex => 11}
  enum conservation: {:perfect => 0, :needs_reform => 1}
  enum furnished: {:yes => 0, :no => 1, :half => 2}

  validates :title, length: {maximum: 100}, allow_nil: false, allow_blank: false
  validates_presence_of :price, :notes, :latitude, :neighborhood_id
  validates :street, length: {maximum: 100}, allow_nil: false, allow_blank: false

  validates :street_number, numericality: { only_integer: false, :greater_than_or_equal => 0}, :allow_nil => true
  validates :total_area, numericality: { only_integer: false, :greater_than_or_equal => 0}, :allow_nil => true
  validates :area_usable, numericality: { only_integer: false, :greater_than_or_equal => 0}, :allow_nil => true
  validates :bathrooms, numericality: { only_integer: false, :greater_than_or_equal => 0}, :allow_nil => true
  validates :bedrooms, numericality: { only_integer: false, :greater_than_or_equal => 0}, :allow_nil => true
  validates :parking_spaces, numericality: { only_integer: false, :greater_than_or_equal => 0}, :allow_nil => true


  scope :search, -> (code, text, priceIni, priceEnd)  {
    w = joins(:neighborhood => [:city => :state]).includes(:neighborhood => [:city => :state]).includes(:real_estate_images).where('real_estates.removed = ?' , false)


    if code.nil? or code.blank?
      unless text.nil? or text.blank?
        w = w.where('real_estates.title like :text or real_estates.notes like :text or real_estates.street like :text or neighborhoods.name like :text or real_states.inside_description like :text or real_states.owner_name like :text', {:text => "%#{text}%"})
      end

      unless priceIni.nil? or priceIni.blank?
        w = w.where('real_estates.price >= ?', priceIni)
      end

      unless priceEnd.nil? or priceEnd.blank?
        w = w.where('real_estates.price <= ?', priceEnd)
      end
    else
      w = w.where(:id => code.gsub(/\D+/, '').to_i)
    end

    w
  }
  scope :total_by_type, ->  {
    where('real_estates.removed = ?' , false).group(:rent_type).count
  }

  scope :search_portal, -> (bairros, type, min_price, max_price, bedrooms, bathrooms, space_vacations)  {
    w = joins(:neighborhood => [:city => :state]).includes(:real_estate_images).where('real_estates.removed = ?' , false)

    unless bairros.nil? or bairros.blank?
      w = w.where('real_estates.neighborhood_id in (?)', bairros.split(','))
    end

    unless type.nil? or type.blank?
      types = type.split(',')
      filter = []

      types.each do |t|
        filter << rent_types[t]
      end

      w = w.where('real_estates.rent_type in (?)', filter)
    end

    unless min_price.nil? or min_price.blank?
      w = w.where('real_estates.price >= ?', min_price.gsub(/\D/, ''))
    end

    unless max_price.nil? or max_price.blank?
      w = w.where('real_estates.price <= ?', max_price)
    end

    unless bedrooms.nil? or bedrooms.blank?
      w = w.where('real_estates.bedrooms in (?)', bedrooms.split(','))
    end

    unless bathrooms.nil? or bathrooms.blank?
      w = w.where('real_estates.bathrooms in (?)', bathrooms.split(','))
    end

    unless space_vacations.nil? or space_vacations.blank?
      w = w.where('real_estates.parking_spaces in (?)', space_vacations.split(','))
    end

    w
  }

  scope :last_added, -> (limit = 4)  {
    joins(:neighborhood => [:city => :state]).where('removed = ?' , false).order('id desc').limit(limit)
  }

  def code
    I18n.t("real_estate_type_#{self.rent_type}")[0..1].upcase + ('%06d' % self.id)
  end


  protected
  def clear_not_number
    self.owner_cpf = owner_cpf.gsub(/\D+/, '') unless owner_cpf.nil?
    self.owner_rg = owner_rg.gsub(/\D+/, '') unless owner_rg.nil?
    self.owner_phone = owner_phone.gsub(/\D+/, '') unless owner_phone.nil?

    self.bookie_phone = bookie_phone.gsub(/\D+/, '') unless bookie_phone.nil?
  end

  # def generate_number
  #   last = RealEstate.select('code').where('id = (select max(id) from real_estates)').lock(true).first
  #   tp = I18n.t("real_estate_type_#{self.rent_type}")[0..1].upcase
  #
  #   if last.nil?
  #     self.code = "#{tp + '000001'}"
  #   else
  #     self.code = tp + ('%06d' % (last.code.gsub(/[^0-9]/, '').to_i + 1))
  #   end
  # end



end
